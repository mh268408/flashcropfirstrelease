//
//  UserData.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 8/5/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CategoryData, DeckData;

@interface UserData : NSManagedObject

@property (nonatomic, retain) NSNumber * autoDeckname;
@property (nonatomic, retain) NSString * cardNamePrefix;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * prioritizeFavorite;
@property (nonatomic, retain) NSNumber * prioritizeWrongCards;
@property (nonatomic, retain) NSNumber * statInNumber;
@property (nonatomic, retain) NSNumber * studyRepeat;
@property (nonatomic, retain) NSNumber * studyShuffle;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) id userPicture;
@property (nonatomic, retain) NSSet *toDeck;
@property (nonatomic, retain) NSSet *toCategory;
@end

@interface UserData (CoreDataGeneratedAccessors)

- (void)addToDeckObject:(DeckData *)value;
- (void)removeToDeckObject:(DeckData *)value;
- (void)addToDeck:(NSSet *)values;
- (void)removeToDeck:(NSSet *)values;

- (void)addToCategoryObject:(CategoryData *)value;
- (void)removeToCategoryObject:(CategoryData *)value;
- (void)addToCategory:(NSSet *)values;
- (void)removeToCategory:(NSSet *)values;

@end
