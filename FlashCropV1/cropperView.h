//
//  cropperView.h
//  FlashCrop
//
//  Created by  on 7/9/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface cropperView : UIView {
@private
    
    UIView *croppingArea;
    
    CGPoint touchStart;
    BOOL isResizing;
}


-(UIImage *) cropImage:(UIImage *) image;
-(UIImage *) cropImageAndBlackOut:(UIImage *) image withFrame:(CGRect) coverFrame;

@property (nonatomic, retain) UIView *contentView;

@property (nonatomic, retain) UIView *rectangle;
@property (nonatomic, retain) UIImageView *topRightCorner;
@property (nonatomic, retain) UIImageView *bottomLeftCorner;
@property (nonatomic, retain) UIImageView *bottomRightCorner;


@end
