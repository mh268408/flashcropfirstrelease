//
//  coreServices.m
//  FlashCrop
//
//  Created by Arkopaul Sarkar on 7/13/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "coreServices.h"


@implementation coreServices

NSManagedObjectContext * _managedObjectContext;
static NSMutableArray *userData;
static NSMutableArray *deckData;       



// getter methods
+(NSMutableArray *)getUserData{
    return userData;
}

+(NSMutableArray *)getDeckData{
    return deckData;
}


//Is user Present method
+(UserData *)isUserPresent:(NSString *)userName{
    
    for (UserData *user in userData) {
        if ([user.userName isEqualToString:userName])
            return user;
    }
    return nil;
}

//Is user Present method
+(DeckData *)isDeckPresent:(NSString *)deckName{
    
    for (DeckData *deck in deckData) {
        if (deck.deckName == deckName)
            return deck;
    }
    return nil;
}


+(NSManagedObject *)instantiateUser{
    
    if (_managedObjectContext == nil)   { 
        _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
    DeckData *deck;
    return deck;
    
}

//create new User if the userName doen't exists (maybe later we can check on email ID)
+(void)createUser:(NSString *)userName
                 :(NSString *)email
                 :(UIImage *)userPicture{
    
    if (_managedObjectContext == nil)   { 
        _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
    UserData *user = [coreServices isUserPresent:userName];
    if (user == nil)
    {
        //Create new Entity for USer
        UserData *user = (UserData *)[NSEntityDescription insertNewObjectForEntityForName:@"UserData" inManagedObjectContext:_managedObjectContext];
        
        user.userName = userName;
        user.email = email;
        user.userPicture = userPicture;
        
        //Now load the default categories
        
        CategoryData *category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"UNASSIGNED";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"ARTS & LITERATURE";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"LANGUAGE & VOCABULARY";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"MATH & SCIENCE";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"HISTORY & GEOGRAPY";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"TESTS";
        [user addToCategoryObject:category];
        
        category = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
        category.category = @"PROFESSIONAL & CAREER";
        [user addToCategoryObject:category];
        
        NSError *error;
        // Save the context.
        if (![_managedObjectContext save:&error]) {

            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
        [coreServices loadData];
    }
    
}

//Method to change settings (populate the NSDictionary with attribute name and value with corresponding flag
+(void)changeSettings:(NSDictionary *)changeValue
                     :(NSDictionary *)changeFlag{
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    //Check is the user is already there and get the instance
    UserData *user = [userData objectAtIndex:0];
    if (user != nil)
    {
        if (_managedObjectContext == nil)   { 
            _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserData" inManagedObjectContext:_managedObjectContext];
        [request setEntity:entity];
        
        
        //fetch the UserData
        NSError *error = nil;
        NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        if (error != nil)
        {
            //handle error 
            NSString *message = [error.description stringByAppendingFormat:[[error userInfo] description]];
            NSLog(@"%@",message);
        }
        // change the entity values
        else {
            user = [mutableFetchResults objectAtIndex:0];
            if([changeFlag objectForKey:@"userName"] == positive)
            {    
                user.userName = [changeValue objectForKey:@"userName"];
            }
            if([changeFlag objectForKey:@"email"] == positive)
            {
                user.email = [changeValue objectForKey:@"email"];
            }
            if([changeFlag objectForKey:@"userPicture"] == positive)
            {
                user.userPicture = [changeValue objectForKey:@"userPicture"];
            }
            if([changeFlag objectForKey:@"autoDeckname"] == positive)
            {
                user.autoDeckname = [changeValue objectForKey:@"autoDeckname"];
            }
            if([changeFlag objectForKey:@"cardNamePrefix"] == positive)
            {
                user.cardNamePrefix = [changeValue objectForKey:@"cardNamePrefix"];
            }
            if([changeFlag objectForKey:@"prioritizeFavorite"] == positive)
            {
                user.prioritizeFavorite = [changeValue objectForKey:@"prioritizeFavorite"];
            }
            if([changeFlag objectForKey:@"prioritizeWrongCards"] == positive)
            {
                user.prioritizeWrongCards = [changeValue objectForKey:@"prioritizeWrongCards"];
            }
            if([changeFlag objectForKey:@"statInNumber"] == positive)
            {
                user.statInNumber = [changeValue objectForKey:@"statInNumber"];
            }
            if([changeFlag objectForKey:@"studyRepeat"] == positive)
            {
                user.studyRepeat = [changeValue objectForKey:@"studyRepeat"];
            }
            if([changeFlag objectForKey:@"studyShuffle"] == positive)
            {
                user.studyShuffle = [changeValue objectForKey:@"studyShuffle"];
            }
            
            NSError *error;
            // Save the context.
            if (![_managedObjectContext save:&error]) { 
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
            
        }
        
    }
    
}

// load the data - only to be called in ApplicationBecomeActive
+(bool)loadData{
    
    //Create fetch Request
    if (_managedObjectContext == nil)   { 
        _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserData" inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    
    //fetch all userdata
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (error != nil)
    {
        //handle error 
        NSString *message = [error.description stringByAppendingFormat:[[error userInfo] description]];
        NSLog(@"%@",message);
    }
    else {
        //For release 1 only one user is applied
        //check the password and other stuff later 
        if ([mutableFetchResults count] > 0){
            NSLog(@"%d", mutableFetchResults.count);
            userData = mutableFetchResults;
            UserData *user = [mutableFetchResults objectAtIndex:0];
            deckData = [[user.toDeck allObjects] mutableCopy];
            
            NSLog(@"Numebr of category %i", [[user toCategory] count] );
            
            NSLog(@"%d", deckData.count);
            
            for (DeckData *deck in user.toDeck) {
                NSLog(@"%@", deck.deckName);
            }
            return true;
        }
        else {
            userData = [[NSMutableArray alloc] init];
            deckData = [[NSMutableArray alloc] init];
        }
    }   
    return false;
}

//selector method for insertcard notification
+(void)InsertCard:(NSString *)deckName 
                 :(NSString *)category
                 :(UIImage *)frontPhoto
                 :(UIImage *)backPhoto
                 :(UIImage *)frontOriginal
                 :(UIImage *)backOriginal
                 :(NSString *)frontText
                 :(NSString *)backText                 
                 :(NSString *)frontContains
                 :(NSString *)backContains
                 :(NSDate *) createdAt{
    bool deckExist;
    DeckData *deckToSaveLocal;
    CardData *cardToSaveLocal;
    
    deckExist = false;
    // check if the deck already exists 
    for (DeckData *deck in deckData) {
        NSLog(@"Deck.deckName %@", deck.deckName);
        NSLog(@"DeckName %@", deckName);
        if ([deck.deckName isEqualToString:deckName])  {
            deckExist = true;
            deckToSaveLocal = deck;    
            break;
        }
    }
    
    if (_managedObjectContext == nil)   { 
        _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserData" inManagedObjectContext:_managedObjectContext];
    [request setEntity:entity];
    
    //Set Predicate
    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName == %@", [[userData objectAtIndex:0] userName]];
    
    [request setPredicate:predicate];
    
    //fetch the UserData
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (error != nil)
    {
        //handle error 
        NSString *message = [error.description stringByAppendingFormat:[[error userInfo] description]];
        NSLog(@"%@",message);
    }
    // user is found
    else {
        
        UserData *user = [mutableFetchResults objectAtIndex:0];
        //Insert a new Deck if the deck Doesn't exists 
        if (!deckExist){
            deckToSaveLocal = (DeckData *)[NSEntityDescription insertNewObjectForEntityForName:@"DeckData" inManagedObjectContext:_managedObjectContext];
        }
        //deckName
        deckToSaveLocal.deckName = deckName;
        //Category
        deckToSaveLocal.category = category;
        //deckNumber
        deckToSaveLocal.deckNumber = [NSNumber numberWithInt:[deckData count]+1];
        
        
        [user addToDeckObject:deckToSaveLocal];
        //create a new Card Object
        cardToSaveLocal = [NSEntityDescription insertNewObjectForEntityForName:@"CardData" inManagedObjectContext:_managedObjectContext];
        
        cardToSaveLocal.frontImage = frontPhoto;
        cardToSaveLocal.frontOriginal = frontOriginal;
        cardToSaveLocal.frontText = frontText;
        cardToSaveLocal.backImage = backPhoto;
        cardToSaveLocal.backOriginal = backOriginal;
        cardToSaveLocal.backText = backText;
        cardToSaveLocal.frontContains = frontContains;
        cardToSaveLocal.backContains = backContains;
        cardToSaveLocal.createdAt = createdAt;
        
        cardToSaveLocal.cardNumber = [NSNumber numberWithInt:[deckToSaveLocal.toCard count]+1];
        
        [deckToSaveLocal addToCardObject:cardToSaveLocal];
        
        NSError *error;
        // Save the context.
        if (![_managedObjectContext save:&error]) {
 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

        }
        [coreServices loadData];
    }
    
}

+(void)insertResult:(DeckData *) deck
                   :(NSNumber *)rightPercent
                   :(NSNumber *)wrongPercent
                   :(NSTimeInterval)timeStudied{
    
    ResultData *result = (ResultData *)[NSEntityDescription insertNewObjectForEntityForName:@"ResultData" inManagedObjectContext:deck.managedObjectContext];
    
    result.timeStamp = [NSDate date];
    result.right = rightPercent;
    result.wrong = wrongPercent;
    result.timeStudied = [NSNumber numberWithInt:(int)timeStudied];
    
    
    NSLog(@"totalRight %f", rightPercent.doubleValue);
    NSLog(@"totalWrong %f", wrongPercent.doubleValue);
    
    [deck addToResultObject:result];
    
    NSError *error;
    // Save the context.
    if (![deck.managedObjectContext save:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    [coreServices loadData];
}


+(void)saveCard:(CardData *)card{
    
    NSError *error = nil;
    if (![card.managedObjectContext save:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    else {
        [coreServices loadData];
    }
    
}

+(void)saveDeck:(DeckData *)deck{
    
    NSError *error = nil;
    if (![deck.managedObjectContext save:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    else {
        [coreServices loadData];
    }
    
}

+(void)saveCategory:(CategoryData *)category{
    
    NSError *error = nil;
    if (![category.managedObjectContext save:&error]) {
 
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    else {
        [coreServices loadData];
    }
    
}

+(bool)addCategory:(NSString *)category{
    
    bool isDuplicate = false;
    
    UserData *user = [userData objectAtIndex:0];
    
    for (CategoryData *cat in user.toCategory){
        if ([cat.category isEqualToString:category]){
            isDuplicate = true;
            return true;
        }
    }
    if (!isDuplicate){
        
        if (_managedObjectContext == nil)   { 
            _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserData" inManagedObjectContext:_managedObjectContext];
        [request setEntity:entity];
        
        //Set Predicate
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"userName == %@", [[userData objectAtIndex:0] userName]];
        
        [request setPredicate:predicate];
        
        //fetch the UserData
        NSError *error = nil;
        NSMutableArray *mutableFetchResults = [[_managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        if (error != nil)
        {
            //handle error 
            NSString *message = [error.description stringByAppendingFormat:[[error userInfo] description]];
            NSLog(@"%@",message);
        }
        // user is found
        else {
            user = [mutableFetchResults objectAtIndex:0];
            
            CategoryData *cat = (CategoryData *)[NSEntityDescription insertNewObjectForEntityForName:@"CategoryData" inManagedObjectContext:_managedObjectContext];
            
            cat.category = category;
            [user addToCategoryObject:cat];
            if (![_managedObjectContext save:&error]) {

                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

            }
            [coreServices loadData];
        }
        
    }
    
    return false;
}

//delete deck from core
+(void)deleteDeck:(NSString *)deckName{
    
    NSManagedObject *deckToDelete;
    
    // get the deck
    for (DeckData *deck in deckData) {
        if([deck.deckName isEqualToString:deckName]){
            deckToDelete = deck;
            break;
        }
    }
    
    [deckToDelete.managedObjectContext deleteObject:deckToDelete];
    
    NSError *error = nil;
    if (![deckToDelete.managedObjectContext save:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    else {
        [coreServices loadData];
    }
    
    
}

+(void)deleteCard:(CardData *)card{
    
    DeckData *deck;
    
    deck = card.toDeck;
    
    // check if this is the last card 
    if (deck.toCard.count <2){
        [coreServices deleteDeck:deck.deckName];
    }
    else{
        [deck removeToCardObject:card];
        NSError *error = nil;
        if (![deck.managedObjectContext save:&error]) {
 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

        }
        else {
            [coreServices loadData];
        }
    }
    
}

+(void)deleteCategory:(NSString *)category{
    
    UserData *user = [[coreServices getUserData] objectAtIndex:0];
    
    for (CategoryData *cat in user.toCategory) {
        if ([cat.category isEqualToString:category]) {
            [user removeToCategoryObject:cat];
            break;
        }
    }
    
    NSError *error = nil;
    if (![user.managedObjectContext save:&error]) {
 
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
    else {
        [coreServices loadData];
    }
    
}

+(void)deleteResult:(ResultData *)result{
    
    DeckData *deck;
    
    deck = result.toDeck;
    
    [deck removeToResultObject:result];
    
    NSError *error = nil;
    if (![deck.managedObjectContext save:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);

    }
}

//format the date
-(NSString *)formatDate:(NSDate *)date 
                       :(NSString *)format{
    
    NSString *formattedDate;
    
    
    
    return formattedDate;
}

// override the managedObjectContext Getter
+ (NSManagedObjectContext *) 
managedObjectContext
{
    if (_managedObjectContext == nil)   { 
        _managedObjectContext = [(fcv1AppDelegate *)[[UIApplication sharedApplication] delegate]managedObjectContext]; }
    return _managedObjectContext;
}


@end
