//
//  helpPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 9/11/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "helpPopUp.h"

@implementation helpPopUp
@synthesize webView;
@synthesize fileName;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



- (IBAction)closePopUp:(id)sender {
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
}

-(void) style{
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] 
                                                                                   pathForResource:fileName ofType:@"html"]isDirectory:NO]]];
    [self.webView stringByEvaluatingJavaScriptFromString:@"window.scrollBy(0,500);"];
    int scrollPosition = [[self.webView stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] intValue];
    NSLog(@"%i", scrollPosition);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
