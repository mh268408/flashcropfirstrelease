
//
//  FolderOptionsViewController.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"
#import "coreServices.h"
#import "namePopUp.h"

@interface FolderOptionsViewController : FCUIView<UITableViewDelegate, UITableViewDataSource, NamePopUpDelegate>
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property namePopUp *namePop;
- (IBAction)editButtonPressed:(id)sender;
- (IBAction)createButtonPressed:(id)sender;
- (IBAction)deleteButtonPressed:(id)sender;

@end