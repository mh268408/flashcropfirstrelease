
//
//  coreServices.h
//  FlashCrop
//
//  Created by Arkopaul Sarkar on 7/13/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserData.h"
#import "DeckData.h"
#import "CardData.h"
#import "CategoryData.h"
#import "ResultData.h"
#import "fcv1AppDelegate.h"
#import "EditCardViewController.h"


@interface coreServices : NSObject

//@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

+(NSMutableArray *)getUserData ;
+(NSMutableArray *)getDeckData ;
+(void)createUser:(NSString *)userName
                 :(NSString *)email
                 :(UIImage *)userPicture;

+(UserData *)isUserPresent:(NSString *)userName;
+(DeckData *)isDeckPresent:(NSString *)deckName;
+(void)changeSettings:(NSDictionary *)changeValue
                     :(NSDictionary *)changeFlag;
+(bool)loadData;
+(NSManagedObject *)instantiateUser; 
+(void)InsertCard:(NSString *)deckName 
                 :(NSString *)category
                 :(UIImage *)frontPhoto
                 :(UIImage *)backPhoto
                 :(UIImage *)frontOriginal
                 :(UIImage *)backOriginal
                 :(NSString *)frontText
                 :(NSString *)backText
                 :(NSString *)frontContains
                 :(NSString *)backContains
                 :(NSDate *) createdAt;
+(void)insertResult:(DeckData *) deck
                   :(NSNumber *)rightPercent
                   :(NSNumber *)wrongPercent
                   :(NSTimeInterval)timeStudied;
+(void)saveCard:(CardData *)card;
+(void)saveDeck:(DeckData *)deck;
+(void)saveCategory:(CategoryData *)category;
+(bool)addCategory:(NSString *)category;
+(void)deleteDeck:(NSString *)deckName;
+(void)deleteCard:(CardData *)card;
+(void)deleteCategory:(NSString *)category;
+(void)deleteResult:(ResultData *)result;
-(NSString *)formatDate:(NSDate *)date 
                       :(NSString *)format;   
//÷(NSMutableArray *)getDeck;



@end