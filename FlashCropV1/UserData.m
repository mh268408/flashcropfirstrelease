//
//  UserData.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 8/5/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "UserData.h"
#import "CategoryData.h"
#import "DeckData.h"


@implementation UserData

@dynamic autoDeckname;
@dynamic cardNamePrefix;
@dynamic email;
@dynamic prioritizeFavorite;
@dynamic prioritizeWrongCards;
@dynamic statInNumber;
@dynamic studyRepeat;
@dynamic studyShuffle;
@dynamic userName;
@dynamic userPicture;
@dynamic toDeck;
@dynamic toCategory;

@end
