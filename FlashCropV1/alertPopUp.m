//
//  alertPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "alertPopUp.h"

@implementation alertPopUp
@synthesize alertMessageLabel;
@synthesize background;
@synthesize content;
@synthesize delegate;
@synthesize buttonOne;
@synthesize buttonTwo;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) style{
    
    content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.75];
    alertMessageLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    
}

- (IBAction)closePopUp:(id)sender {
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] alertPopUpFinished:false];
}

- (IBAction)confirm:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] alertPopUpFinished:true];
    
}
@end
