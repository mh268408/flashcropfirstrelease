//
//  MyCardsViewController.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "coreServices.h"
#import "FCTableViewController.h"
#import "ViewCardsViewController.h"
#import "deckPopUp.h"
#import "helpPopUp.h"

@interface MyCardsViewController : FCTableViewController <DeckPopDelegate>

@property (nonatomic, strong) deckPopUp *editDeckPopUp;
@property (nonatomic, strong) helpPopUp *helpPop;

- (IBAction)helpPressed:(id)sender;

@end
