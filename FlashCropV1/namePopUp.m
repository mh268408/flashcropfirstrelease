//
//  namePopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "namePopUp.h"

@implementation namePopUp
@synthesize textBox;
@synthesize content;
@synthesize background;
@synthesize closeButton;
@synthesize title;
@synthesize button1;
@synthesize button2;
@synthesize delegate;
@synthesize type;
@synthesize alertPop;
@synthesize cancel;
@synthesize button1Title;
@synthesize button2Title;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) style{
    self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    self.background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.textBox.delegate = self;
    
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"alertPopUp" owner:self options:nil];
    self.alertPop = [subviewArray objectAtIndex:0];
    [self.alertPop style];
    [self addSubview:self.alertPop];
    self.alertPop.alpha = 0;
    self.alertPop.alertMessageLabel.text = @"PLEASE ENTER SOME TEXT";
    self.cancel = false;
    self.button1.titleLabel.text = self.button1Title;
    self.button2.titleLabel.text = self.button2Title;
    
    
    
}

- (IBAction)closePopUp:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [self.title resignFirstResponder];
}

- (IBAction)button1Pressed:(id)sender {
    NSString *strippedString = [self.textBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(strippedString.length == 0){
        [[self delegate] NamePopUpFinished:strippedString :false];
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        self.alertPop.alpha = 1;
        [UIView commitAnimations];
        self.button1.titleLabel.text = button1Title;
    }
    else{
        [[self delegate] NamePopUpFinished:strippedString :true];
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        self.alpha = 0;
        [UIView commitAnimations];
        [self.title resignFirstResponder];
    }
    
    [self.textBox resignFirstResponder];
}

- (IBAction)button2Pressed:(id)sender {
    NSString *strippedString = [self.textBox.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(strippedString.length == 0 && cancel == false){
        
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        self.alertPop.alpha = 1;
        [UIView commitAnimations];
        
        self.button2.titleLabel.text = button2Title;
    }
    else{
        [[self delegate] NamePopUpFinished:strippedString :false];
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        self.alpha = 0;
        [UIView commitAnimations];
        [textBox resignFirstResponder];
    }
    
    [self.textBox resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


@end
