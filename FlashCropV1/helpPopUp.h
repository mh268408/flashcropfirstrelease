//
//  helpPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 9/11/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface helpPopUp : UIView
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property NSString *fileName;
- (IBAction)closePopUp:(id)sender;

-(void) style;
@end
