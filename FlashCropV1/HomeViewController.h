//
//  HomeViewController.h
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PieChartView.h"
#import "FCUIView.h"
#import "coreServices.h"
#import "welcomePopUp.h"
#import "helpPopUp.h"

@interface HomeViewController : FCUIView <UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate>{
    PieChartView *_pieChart;
}
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *usernameBorder;
@property (weak, nonatomic) IBOutlet UITextView *greetingMessage;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (nonatomic, retain) IBOutlet PieChartView *pieChart;
@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (strong, nonatomic) IBOutlet UIView *welcomePopOver;
@property (strong, nonatomic) IBOutlet UIView *welcomeContentView;
@property (strong, nonatomic) IBOutlet UITextField *nameBox;
@property (strong, nonatomic) IBOutlet UITextField *emailBox;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (weak, nonatomic) IBOutlet UILabel *daysSince;
@property (weak, nonatomic) IBOutlet UILabel *correctLabel;
@property (weak, nonatomic) IBOutlet UILabel *incorrectLabel;
@property (weak, nonatomic) IBOutlet UILabel *daySinceText;
@property (weak, nonatomic) helpPopUp *helpPop;
- (IBAction)choosePictureButtonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)skipButtonPressed:(id)sender;
- (IBAction)helpPressed:(id)sender;

@end
