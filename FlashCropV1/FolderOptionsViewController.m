//
//  FolderOptionsViewController.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FolderOptionsViewController.h"

@interface FolderOptionsViewController ()

@end

@implementation FolderOptionsViewController
@synthesize backgroundView;
@synthesize containerView;
@synthesize table;
@synthesize namePop;

NSMutableArray *categoryList;
namePopUp *namePop;
bool isEdit;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.back = true;
    self.navBar = true;
    
    self.title = @"FOLDER OPTIONS";
    self.table.delegate =  self;
    self.table.dataSource = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarSettings.png"]]  atIndex:0]; 
    
    // load categories
    UserData *user = [[coreServices getUserData] objectAtIndex:0];
    categoryList = [[user.toCategory allObjects] mutableCopy];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%i", categoryList.count);
    return categoryList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [[categoryList objectAtIndex:indexPath.row] category];
    
    
    return cell;
}



- (IBAction)editButtonPressed:(id)sender {
    
    if (self.table.indexPathForSelectedRow != nil){ 
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"namePopUp" owner:self options:nil];
        self.namePop = [subviewArray objectAtIndex:0];
        self.namePop.delegate = self;
        [self.namePop style];
        self.namePop.title.text = @"CHANGE CATEGORY";
        self.namePop.button1.titleLabel.text = @"DONE";
        self.namePop.button2.titleLabel.text = @"CANCEL";
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        [self.view addSubview:self.namePop];
        //    self.content.alpha = 0;
        self.namePop.alpha = 1;
        [UIView commitAnimations];
        isEdit = true;
    }
}

- (IBAction)createButtonPressed:(id)sender {
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"namePopUp" owner:self options:nil];
    self.namePop = [subviewArray objectAtIndex:0];
    self.namePop.delegate = self;
    [self.namePop style];
    self.namePop.title.text = @"ADD CATEGORY";
    self.namePop.button1.titleLabel.text = @"DONE";
    self.namePop.button2.titleLabel.text = @"CANCEL";
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    [self.view addSubview:self.namePop];
    //    self.content.alpha = 0;
    self.namePop.alpha = 1;
    [UIView commitAnimations];
    isEdit  = false;
}

- (IBAction)deleteButtonPressed:(id)sender {
    
    bool isUsed = false;
    if (self.table.indexPathForSelectedRow != nil) {
        //check if the category is used by any existing Deck
        for (DeckData *deck in [coreServices getDeckData]) {
            if ([deck.category isEqualToString:[[categoryList objectAtIndex:self.table.indexPathForSelectedRow.row] category]]) {
                //alert that this category can not be deleted because it is already used in a deck deck data found in .deck
                
                isUsed = true;
                break;
            }
        }
        
        if (!isUsed) {
            //alert that this category will be deleted permanently
            
            //delete category from coredata
            [coreServices deleteCategory:[[categoryList objectAtIndex:self.table.indexPathForSelectedRow.row] category]];
            //delete category from datasource
            [categoryList removeObjectAtIndex:self.table.indexPathForSelectedRow.row];
            [self.table reloadData];
        }
    }
    
}

//delegate for namePOpup
- (void) NamePopUpFinished:(NSString *)textExtract :(BOOL)buttonIndex{
    
    if(!isEdit)
    {
        //creating new Category
        bool isduplicate = [coreServices addCategory:textExtract];
        
        if (!isduplicate)
        {
            //change the datasource
            UserData *user = [[coreServices getUserData] objectAtIndex:0];
            categoryList = [[user.toCategory allObjects] mutableCopy];
            [self.table reloadData];
        }
    }
    else {
        CategoryData *cat = [categoryList objectAtIndex:self.table.indexPathForSelectedRow.row];
        
        //change categories of all the decks
        for (DeckData *deck in [coreServices getDeckData]) {
            if ([deck.category isEqualToString:cat.category]) {
                deck.category = textExtract;
                [coreServices saveDeck:deck];
            }
        }
        //now change the category in category data
        cat.category = textExtract;
        
        [coreServices saveCategory:cat];
        //change the datasource
        UserData *user = [[coreServices getUserData] objectAtIndex:0];
        categoryList = [[user.toCategory allObjects] mutableCopy];        
        [self.table reloadData];
    }
}
@end


