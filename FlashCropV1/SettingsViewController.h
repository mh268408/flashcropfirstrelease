//
//  SettingsViewController.h
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "FCUIView.h"
#import "helpPopUp.h"
#import "GANTracker.h"

@interface SettingsViewController : FCUIView <ADBannerViewDelegate>
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *closeAdButton;
@property (nonatomic, strong) helpPopUp *helpPop;
- (IBAction)helpPushed:(id)sender;
- (IBAction)closeAd:(id)sender;

@end
