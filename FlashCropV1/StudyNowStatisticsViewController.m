//
//  StudyNowStatisticsViewController.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "StudyNowStatisticsViewController.h"

@interface StudyNowStatisticsViewController ()

@end

@implementation StudyNowStatisticsViewController
@synthesize backgroundView;
@synthesize containerView;
@synthesize shuffleCheckBox;
@synthesize startFromBeginingCheckBox;
@synthesize prioritizeIncorrectCheckBox;
@synthesize prioritizeFavoriteCheckBox;
@synthesize green;
@synthesize grey;
@synthesize orange;

bool shuffle, start, prior, favorite;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.back = true;
    self.navBar = true;
    
    self.grey = [UIColor colorWithRed:0.388 green:0.329 blue:0.4 alpha:1];
    self.green = [UIColor colorWithRed:0.184 green:0.533 blue:0.277 alpha:1];
    self.orange = [UIColor colorWithRed:0.96 green:0.501 blue:0.203 alpha:1];
    self.title = @"STUDY NOW";
    [super viewDidLoad];
	}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    // Do any additional setup after loading the view.
    if([[[[coreServices getUserData] objectAtIndex:0] studyShuffle] isEqualToNumber:[NSNumber numberWithInt:1]]){
        shuffle  = true;         
    }
    else {
        shuffle  =  false;
    }
    if ([[[[coreServices getUserData] objectAtIndex:0] studyRepeat] isEqualToNumber:[NSNumber numberWithInt:1]]){
        start = true;
    }
    else {
        start  = false;
    }
    if([[[[coreServices getUserData] objectAtIndex:0] prioritizeWrongCards] isEqualToNumber:[NSNumber numberWithInt:1]]){
        prior = true;
    }
    else {
        prior =  false;
    }
    if([[[[coreServices getUserData] objectAtIndex:0] prioritizeFavorite] isEqualToNumber:[NSNumber numberWithInt:1]]){
        favorite = true;
    }
    else {
        favorite  = false;
    }
    
    if(shuffle){
        self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        
    }
    else{
        self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        
    }
    //blocked for the current version Arko 09/12/12 block removed on 9/16/2012
    if(start){
        self.startFromBeginingCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
    }
    else{
        self.startFromBeginingCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        
    }
    if(prior){
        self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
    }
    else{
        self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        
    }
    if(favorite){
        self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
    }
    else{
        self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        
    }
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarSettings.png"]]  atIndex:0]; 
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];    [self setShuffleCheckBox:nil];
    [self setStartFromBeginingCheckBox:nil];
    [self setPrioritizeIncorrectCheckBox:nil];
    [self setPrioritizeFavoriteCheckBox:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)shuffleCheckBoxPressed:(id)sender {
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    NSDictionary *changeValue;
    if(shuffle){
        self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"studyShuffle", 
                                     nil];
    }
    else{
        self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        positive, @"studyShuffle", 
                                     nil];
        //if prioritize buttons are checked, uncheck them
        if(prior){
            self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeWrongCards", 
                           nil]; 
            prior = !prior;
        }
        if(favorite){
            self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeFavorite", 
                           nil];
            favorite =  !favorite;
        }
    }
    shuffle = !shuffle;
    
    //save Settings in core service
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"studyShuffle",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
}

- (IBAction)startFromBeginningCheckBoxPressed:(id)sender {
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    NSDictionary *changeValue;
    if(start){
        self.startFromBeginingCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"studyRepeat", 
                       nil];
    }
    else{
        self.startFromBeginingCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        positive, @"studyRepeat", 
                       nil];
        
    }
    start = !start;
    
    //save Settings in core service
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"studyRepeat",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
}

- (IBAction)prioritizeIncorrectCheckBoxPressed:(id)sender {
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    NSDictionary *changeValue;
    if(prior){
        self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeWrongCards", 
                       nil];        
    }
    else{
        self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        positive, @"prioritizeWrongCards", 
                       nil]; 
        //if favourite is checked uncheck it
        if(favorite){
            self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeFavorite", 
                           nil];
            favorite = !favorite;
        }
        //if shuffle is checked turn that off
        if (shuffle) {
            self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"studyShuffle", 
                           nil];
            shuffle = !shuffle;
        }
        
    }
    prior = !prior;
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"prioritizeWrongCards",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];

}

- (IBAction)prioritizeFavoriteCheckBoxPressed:(id)sender {
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    NSDictionary *changeValue;
    if(favorite){
        self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeFavorite", 
                       nil];
    }
    else{
        self.prioritizeFavoriteCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        positive, @"prioritizeFavorite", 
                       nil];
        //if incorrect priority is checked uncheck it
        if(prior){
            self.prioritizeIncorrectCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"prioritizeWrongCards", 
                           nil];
            
            prior = !prior;
        }
        //if shuffle is checked turn that off
        if (shuffle) {
            self.shuffleCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
            changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"studyShuffle", 
                           nil];
            shuffle = !shuffle;
        }
    }
    favorite = !favorite;
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"prioritizeFavorite",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];

}
@end
