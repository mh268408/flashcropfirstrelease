//
//  FCPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol FCPopUpDelegate <NSObject>
@required
- (void) FCPopUpFinished: (int)buttonIndex;
@end


@interface FCPopUp : UIView
{
	id <FCPopUpDelegate> delegate;
}

@property (retain) id delegate;
@property IBOutlet UIView *background;
@property IBOutlet UIView *content;
@property (strong, nonatomic) IBOutlet UIButton *buttonOne;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwo;
@property (strong, nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UILabel *title;

- (IBAction)closeButton:(id)sender;
- (void) style;
- (IBAction)button1Action:(UIButton *)sender;
- (IBAction)button2Action:(UIButton *)sender;
- (IBAction)button3Action:(UIButton *)sender;


@end