//
//  FCUIView.h
//  FlashCropV1
//
//  Created by Max Heckel on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCUIView : UIViewController

@property (nonatomic, strong) UIView * backgroundView;
@property (nonatomic, strong) UIView * containerView;
@property BOOL navBar;
@property BOOL back;

@end
