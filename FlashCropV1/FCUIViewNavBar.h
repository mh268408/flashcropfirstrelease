//
//  FCUIViewNavBar.h
//  FlashCropV1
//
//  Created by Max Heckel on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "<#header#>"

@interface FCUIViewNavBar : UIViewController

@end
