//
//  categoryPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "namePopUp.h"
#import "coreServices.h"
#import "alertPopUp.h"
#import <QuartzCore/QuartzCore.h>

@protocol CategoryDelegate <NSObject>
@required
- (void) categoryFinished: (NSString *)textExtract;
@end

@interface categoryPopUp : UIView <NamePopUpDelegate, UITableViewDelegate, UITableViewDataSource>
{
	id <CategoryDelegate> delegate;
}
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (retain) id delegate;
@property (strong, nonatomic) IBOutlet UIView *content;
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UITableView *table;
@property namePopUp *namePop;
@property alertPopUp *alertPop;
@property NSString *mode;
@property NSString *addButtonMode;
- (IBAction)selectButtonPushed:(id)sender;
- (IBAction)addButtonPushed:(id)sender;
- (IBAction)closePopUp:(id)sender;
- (void) style;



@end
