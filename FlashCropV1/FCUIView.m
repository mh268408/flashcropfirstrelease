//
//  FCUIView.m
//  FlashCropV1
//
//  Created by Max Heckel on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"

@interface FCUIView ()

@end

@implementation FCUIView
@synthesize backgroundView;
@synthesize containerView;
@synthesize navBar;
@synthesize back;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackgroundNavPushedDown.png"]];
    self.containerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeContainer.png"]];
    
    if(self.navBar){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:41];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor =[UIColor whiteColor];
        label.shadowColor = [UIColor colorWithWhite:0 alpha:1];
        label.shadowOffset = CGSizeMake(1, 1);
        label.text=self.title;
        [label sizeToFit];
//        if(self.title != @"SETTINGS" && self.title != @"MY CARDS" && self.title != @"DECK NAME"){
//            label.textAlignment = UITextAlignmentLeft; 
//        }
//        
        self.navigationItem.titleView = label;     
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavBackground.png"] forBarMetrics:UIBarMetricsDefault];
        self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackgroundNav.png"]];
        if(self.back){
            if([self.navigationController.viewControllers objectAtIndex:0] != self)
            {
                UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 54, 30)];
                [backButton setImage:[UIImage imageNamed:@"backButtonBackground.png"] forState:UIControlStateNormal];
                [backButton setShowsTouchWhenHighlighted:TRUE];
                [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
                backButton.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:16.67];
                backButton.titleLabel.textColor = [UIColor colorWithRed:237 green:126 blue:56 alpha:1];
                UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
                self.navigationItem.hidesBackButton = TRUE;
                self.navigationItem.leftBarButtonItem = barBackItem;
            }
        }
        
        self.title = nil;
    }
    else {
        
        self.tabBarController.tabBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    }
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
