//
//  deckPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/16/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "deckPopUp.h"

@implementation deckPopUp
@synthesize background;
@synthesize content;
@synthesize tableView;
@synthesize title1;
@synthesize title2;
@synthesize changeDeckTextField;
@synthesize button1;
@synthesize button2;
@synthesize table;
@synthesize delegate;

NSMutableArray *categoryArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) style{
    self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    self.background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.changeDeckTextField.delegate = self;
    
    self.table.delegate =  self;
    self.table.dataSource = self;
    self.changeDeckTextField.delegate = self;

    UserData *user = [[coreServices getUserData] objectAtIndex:0];
    categoryArray = [[user.toCategory allObjects] mutableCopy];
}

- (IBAction)button1Pressed:(id)sender {
    self.changeDeckTextField.text = [self.changeDeckTextField.text uppercaseString];
    [[self delegate] deckPopFinished:self.changeDeckTextField.text :[[categoryArray objectAtIndex:self.table.indexPathForSelectedRow.row] category]];
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    
}



- (IBAction)closePopUp:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%i", categoryArray.count);
    return categoryArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [[categoryArray objectAtIndex:indexPath.row] category];
    
    return cell;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
