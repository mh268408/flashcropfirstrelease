//
//  CreateCardsViewController.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "CreateCardsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CreateCardsViewController ()

@end

@implementation CreateCardsViewController{
    ADBannerView *_bannerView;
}

@synthesize backgroundView;
@synthesize containerView;
@synthesize nameButton;
@synthesize categoryButton;
@synthesize doneButton;
@synthesize topCardNumber;
@synthesize pageTitle;
@synthesize choosePopUp;
@synthesize deckNameSelector;
@synthesize enterText;
@synthesize categoryPop;
@synthesize alertPop;
@synthesize picker;
@synthesize image;
@synthesize frontCardView;
@synthesize frontCardImageView;
@synthesize frontCardLabel;
@synthesize backCardView;
@synthesize backCardImageView;
@synthesize backCardLabel;
@synthesize mode;
@synthesize cardSavedPopUp;
@synthesize cropViewFront;
@synthesize cropViewBack;
@synthesize cropButton;
@synthesize resetButton;
@synthesize croppingBackground;
@synthesize frontBackButton;
@synthesize deckNameTapGesture;
@synthesize cardForEdit;
@synthesize back;
@synthesize front;
@synthesize frontCropLabel;
@synthesize backCropLabel;
@synthesize exitCropButton;
@synthesize helpPop;
@synthesize closeAdButton;

//private
bool buttonLabel;
bool isFrontImage = true;    //true when front of card is an image // false when text
bool isBackImage = true;      //true when back of card is an image // false when text
bool isFrontCardLoaded = false; //true when something is there on front 
bool isBackCardLoaded = false; //true when soemthing is there on back
bool sourceIsCamera;
bool isRetina;
bool isNewDeck = false;
bool isImagePickerCalled = false;

NSString *categoryMode;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    //Ads
#ifdef LITE_VER
    _bannerView = [[ADBannerView alloc] init];
    _bannerView.delegate = self;
    _bannerView.frame=CGRectMake(0, 480, 320, 50);
    [self.view addSubview:_bannerView];
#endif
    
    self.closeAdButton.alpha=0;
    //google analytics page view
    NSError *error;
    if (![self.mode isEqualToString:@"Edit"]){
        if (![[GANTracker sharedTracker] trackPageview:@"/createcard_visit"
                                             withError:&error]) {
            // Handle error here
        }
    }
    else {
        if (![[GANTracker sharedTracker] trackPageview:@"/mycard_visit/viewcard_visit/createcard_visit"
                                             withError:&error]) {
            // Handle error here
        }
    }
    
    isRetina = ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?1:0;
    self.cardSavedPopUp.alpha = 0;
    self.cropButton.alpha = 0;
    self.resetButton.alpha = 0;
    
    self.exitCropButton.hidden = TRUE;
    self.categoryButton.titleLabel.textAlignment = UITextAlignmentCenter;
    self.nameButton.titleLabel.textAlignment = UITextAlignmentCenter;
    
    self.title = nil;
    //navbar info button
    
    //Create the front and back of the card initial button
    //add gesture recognizer
    
    self.pageTitle.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NavBackground.png"]];
    UITapGestureRecognizer *frontFingerTap = 
    [[UITapGestureRecognizer alloc] initWithTarget:self 
                                            action:@selector(handleClickOnFront:)];
    [self.frontCardView addGestureRecognizer:frontFingerTap];
    
    UITapGestureRecognizer *backFingerTap = 
    [[UITapGestureRecognizer alloc] initWithTarget:self 
                                            action:@selector(handleClickOnBack:)];
    [self.backCardView addGestureRecognizer:backFingerTap];
    
    //Initialization of popOvers
    // FCPopUp for choosing source for front and back of card
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"FCPopUp" owner:self options:nil];
    self.choosePopUp = [subviewArray objectAtIndex:0];
    [self.choosePopUp style];
    self.choosePopUp.title.text = @"CHOOSE SOURCE";
    self.choosePopUp.delegate = self;
    [self.view addSubview:self.choosePopUp];
    self.choosePopUp.alpha = 0;
    
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"helpView" owner:self options:nil];
    self.helpPop = [subviewArray objectAtIndex:0];
    self.helpPop.fileName = @"CREATEHELP";
    [self.helpPop style];
    self.helpPop.alpha = 0;
    [self.view addSubview:self.helpPop];
    
    //namepopup for for typing new deck name
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"namePopUp" owner:self options:nil];
    self.deckNameSelector = [subviewArray objectAtIndex:0];
    [self.deckNameSelector style];
    self.deckNameSelector.title.text = @"DECK NAME";
    self.deckNameSelector.button1.titleLabel.text = @"CHANGE NAME";
    self.deckNameSelector.button2.titleLabel.text = @"CREATE NEW";
    [self.view addSubview:deckNameSelector];
    self.deckNameSelector.delegate = self;
    self.deckNameSelector.alpha = 0;
	// Do any additional setup after loading the view.
    
    //entertext for the new text
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"enterTextPopUp" owner:self options:nil];
    self.enterText = [subviewArray objectAtIndex:0];
    [self.enterText style];
    self.enterText.delegate = self;
    [self.view addSubview:self.enterText];
    self.enterText.alpha = 0;
    
    
    deckNameTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.deckNameSelector addGestureRecognizer:deckNameTapGesture];
    self.deckNameSelector.button1.userInteractionEnabled = NO;
    self.deckNameSelector.button2.userInteractionEnabled = NO;
    
    //alert pop ove
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"alertPopUp" owner:self options:nil];
    self.alertPop = [subviewArray objectAtIndex:0];
    [self.alertPop style];
    [self.view addSubview:self.alertPop];
    self.alertPop.alpha = 0;
    self.frontBackButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frontEnabledBackground.png"]];
    self.frontBackButton.hidden = true;    
    self.front = true;

    //Define mode
    if ([self.mode isEqualToString:@"Edit"]){        //populate the card view for editing
        // pass the card
        
        self.exitCropButton.hidden = TRUE;
        self.nameButton.hidden = YES;
        self.categoryButton.hidden = YES;
        self.pageTitle.hidden = YES;
        self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackgroundNav.png"]];
        self.containerView.frame = CGRectMake(9, -46, 302, 500);
        self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangleEmpty.png"]];
        self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangleEmpty.png"]];
        self.frontCardView.frame = CGRectMake(16, 58, 272, 137);
        self.backCardView.frame = CGRectMake(16, 205, 272, 137);
        self.doneButton.frame = CGRectMake(87, 356, 128, 37);
        self.choosePopUp.content.frame = CGRectMake(25, 78, 270, 325);
        self.title=@"EDIT CARD";
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:41];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = UITextAlignmentCenter;
        label.textColor =[UIColor whiteColor];
        label.shadowColor = [UIColor colorWithWhite:0 alpha:1];
        label.shadowOffset = CGSizeMake(1, 1);
        label.text=self.title;
        [label sizeToFit];
        self.navigationItem.titleView = label; 
        
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 54, 30)];
        [backButton setImage:[UIImage imageNamed:@"backButtonBackground.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        backButton.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:16.67];
        backButton.titleLabel.textColor = [UIColor colorWithRed:237 green:126 blue:56 alpha:1];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;

        self.nameButton.titleLabel.text = self.cardForEdit.toDeck.deckName;
        self.categoryButton.titleLabel.text = self.cardForEdit.toDeck.category;
        //front card
        isFrontCardLoaded = YES;
        if([self.cardForEdit.frontContains isEqualToString:@"image"]){
            self.frontCardImageView.hidden = NO;
            self.frontCardLabel.hidden = YES;
            self.frontCardImageView.image = self.cardForEdit.frontImage;
            isFrontImage = YES;
        }
        else if([self.cardForEdit.frontContains isEqualToString:@"text"]){
            self.frontCardImageView.hidden = YES;
            self.frontCardLabel.hidden = NO;
            self.frontCardLabel.text = self.cardForEdit.frontText;
            isFrontImage = NO;
        }
        
        //backCard
        isBackCardLoaded = YES;
        if([self.cardForEdit.backContains isEqualToString:@"image"]){
            self.backCardImageView.hidden = NO;
            self.backCardLabel.hidden = YES;
            self.backCardImageView.image = self.cardForEdit.backImage;
            isBackImage = YES;
            
        }
        else if([self.cardForEdit.backContains isEqualToString:@"text"]){
            self.backCardImageView.hidden = YES;
            self.backCardLabel.hidden = NO;
            self.backCardLabel.text = self.cardForEdit.backText;
            isBackImage = NO;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
#ifdef LITE_VER
    if(_bannerView.bannerLoaded){
        _bannerView.hidden=false;
        self.closeAdButton.alpha=1;
    }
#endif
    if(![self.mode isEqualToString:@"Edit"]){
        self.mode = @"Create";
        //autodeck Name
        UserData *user = [[coreServices getUserData] objectAtIndex:0];
        NSNumber *positive = [[NSNumber alloc] initWithInteger:1]; 
        if (user.autoDeckname == positive && !isImagePickerCalled){
            NSString *deckName = [user.cardNamePrefix stringByAppendingFormat:[[NSString alloc] initWithFormat:@" %i",[[coreServices getDeckData] count]+1]];
            deckName = [deckName uppercaseString];
            self.nameButton.titleLabel.text = deckName;
        }
        for(UIView *view in self.tabBarController.tabBar.subviews) {  
            if([view isKindOfClass:[UIImageView class]]) {  
                [view removeFromSuperview];  
            }  
        }  
        
        [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarCreateCards.png"]]  atIndex:0]; 
    }
    else{
        for(UIView *view in self.tabBarController.tabBar.subviews) {  
            if([view isKindOfClass:[UIImageView class]]) {  
                [view removeFromSuperview];  
            }  
        }  
        
        [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarMyCards.png"]]  atIndex:0]; 
    }
    isImagePickerCalled = false;
    if (![self.mode isEqualToString:@"Edit"]){
        self.mode = @"Create";
    }
}


-(void)hideKeyboard{
    
    [self.deckNameSelector.textBox resignFirstResponder];
    [self.deckNameSelector removeGestureRecognizer:self.deckNameTapGesture];
    self.deckNameSelector.button1.userInteractionEnabled = YES;
    self.deckNameSelector.button2.userInteractionEnabled = YES;
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setNameButton:nil];
    [self setCategoryButton:nil];
    [self setDoneButton:nil];
    [self setTopCardNumber:nil];
    [self setFrontCardView:nil];
    [self setFrontCardLabel:nil];
    [self setCropButton:nil];
    [self setResetButton:nil];
    [self setCardSavedPopUp:nil];
    [self setPageTitle:nil];
    [self setFrontBackButton:nil];
    [self setCloseAdButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




- (IBAction)nameButtonPressed:(id)sender {

    if (![self.mode isEqualToString:@"Edit"]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"categoryPopUp" owner:self options:nil];
        self.categoryPop = [subviewArray objectAtIndex:0];
        self.categoryPop.mode = @"Deck";
        self.categoryPop.button2.titleLabel.text = @"ADD NEW DECK";
        self.categoryPop.button1.titleLabel.text = @"SELECT DECK";
        self.categoryPop.addButtonMode = @"Add";
        [self.categoryPop style];
        self.categoryPop.delegate = self;
        self.categoryPop.label.text = @"DECKS";
        [self.view addSubview:self.categoryPop];
        self.categoryPop.alpha = 0;
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        self.categoryPop.alpha = 1;
        [UIView commitAnimations];
        categoryMode = @"Deck";
    }
    
}

- (IBAction)categoryButton:(id)sender {
    
    //category pop over only opos if the deck is brand new (no change of category for existing deck)
    isNewDeck = false;
    for (DeckData *deck in [coreServices getDeckData]) {
        if ([deck.deckName isEqualToString:self.nameButton.titleLabel.text]) {
            isNewDeck = true;
            break;
        }
    }
    if(!isNewDeck){
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"categoryPopUp" owner:self options:nil];
        self.categoryPop = [subviewArray objectAtIndex:0];
        self.categoryPop.mode = @"Category";
        self.categoryPop.button2.titleLabel.text = @"ADD NEW CATEGORY";
        self.categoryPop.button1.titleLabel.text = @"SELECT CATEGORY";
        self.categoryPop.addButtonMode = @"Add";
        [self.categoryPop style];
        self.categoryPop.delegate = self;
        self.categoryPop.label.text = @"CATEGORIES";
        [self.view addSubview:self.categoryPop];
        self.categoryPop.alpha = 0;
        [UIView beginAnimations:@"fade in" context:nil];
        [UIView setAnimationDuration:0.5];
        self.categoryPop.alpha = 1;
        [UIView commitAnimations];
        categoryMode = @"Category";
    }
}


- (IBAction)saveCard:(UIButton *)sender {
    
    NSString *label;
    bool permitted = true;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.cardSavedPopUp.alpha = 1;
    } completion:^(BOOL finished){
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.2];
        self.cardSavedPopUp.alpha = 0;
        [UIView commitAnimations];
        
    }];
    if ([self.mode isEqualToString:@"Create"]) {
        // create card
//#ifdef LITE_VER
//        int count = 0;
//        //check if limit of cards for free version is reached
//        for (DeckData *deck in [coreServices getDeckData]) {
//            count = count + deck.toCard.count;
//        }
//        if (count > 5) {
//            [UIView beginAnimations:@"fade out" context:nil];
//            [UIView setAnimationDuration:0.5];
//            self.alertPop.alertMessageLabel.text = @"Only 50 cards are allowed in the lite version";
//            self.alertPop.alpha = 1;
//            [UIView commitAnimations];
//            permitted = false;
//        }
//        else{
//            permitted = true;
//        }
//#endif        
        
        if (!isFrontCardLoaded){
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            self.alertPop.alertMessageLabel.text = @"Load the front of the card";
            self.alertPop.alpha = 1;
            [UIView commitAnimations]; 
        }
        if (!isBackCardLoaded){
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            self.alertPop.alertMessageLabel.text = @"Load the back of the card";
            self.alertPop.alpha = 1;
            [UIView commitAnimations]; 
        }
        if (permitted) {
            if (isBackCardLoaded && isFrontCardLoaded){
                UIImage *nilImage = [[UIImage alloc] init];
                NSString *nilString = [[NSString alloc] init];
                if (isFrontImage && isBackImage) {
                    [coreServices InsertCard:self.nameButton.titleLabel.text :self.categoryButton.titleLabel.text :self.frontCardImageView.image :backCardImageView.image :nilImage :nilImage :nilString :nilString :@"image" :@"image":[NSDate date]];
                    label = @"frontimagebackimage";
                }
                if (!isFrontImage && !isBackImage){
                    [coreServices InsertCard:self.nameButton.titleLabel.text :self.categoryButton.titleLabel.text :nilImage :nilImage :nilImage :nilImage :self.frontCardLabel.text :backCardLabel.text :@"text" :@"text":[NSDate date]];
                    label = @"fronttextbacktext";
                }
                if (isFrontImage && !isBackImage){
                    [coreServices InsertCard:self.nameButton.titleLabel.text :self.categoryButton.titleLabel.text :self.frontCardImageView.image :nilImage :nilImage :nilImage :nilString :backCardLabel.text :@"image" :@"text":[NSDate date]];
                    label = @"frontimagebacktext";
                }
                if (!isFrontImage && isBackImage){
                    [coreServices InsertCard:self.nameButton.titleLabel.text :self.categoryButton.titleLabel.text :nilImage :self.backCardImageView.image :nilImage :nilImage :self.frontCardLabel.text :nilString   :@"text" :@"image":[NSDate date]] ;
                    label = @"fronttextbackimage";
                }
                
                //track event for editing deck name
                NSError *error;
                if (![[GANTracker sharedTracker] trackEvent:@"createcard"
                                                     action:@"savecard"
                                                      label:label
                                                      value:-1
                                                  withError:&error]) {
                    NSLog(@"Error: %@", error);
                }
                
                //Add animation to flash "saved"
                
                //restore the initial state of front and back card
                self.frontCardImageView.hidden = true;
                self.frontCardLabel.hidden = true;
                self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangle.png"]];
                self.backCardImageView.hidden = true;
                self.backCardLabel.hidden = true;
                self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangle.png"]];
                
            }
        }
    }
    else {
        //edit card 
        if (isFrontImage && isBackImage) {
            self.cardForEdit.frontImage = self.frontCardImageView.image;
            self.cardForEdit.backImage = self.backCardImageView.image;
        }
        if (!isFrontImage && !isBackImage){
            self.cardForEdit.frontText = self.frontCardLabel.text;
            self.cardForEdit.backText = self.backCardLabel.text;
        }
        if (isFrontImage && !isBackImage){
            self.cardForEdit.frontImage = self.frontCardImageView.image;
            self.cardForEdit.backText = self.backCardLabel.text;
        }
        if (!isFrontImage && isBackImage){
            self.cardForEdit.frontText = self.frontCardLabel.text;
            self.cardForEdit.backImage = self.backCardImageView.image;
        }
        [coreServices saveCard:cardForEdit];
        //pass updated card back to the last view controller
        NSArray *viewControllers = self.navigationController.viewControllers;
        ViewCardsViewController *previousController = [viewControllers objectAtIndex:[viewControllers count] - 2];
        previousController.card = self.cardForEdit;
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}   

- (IBAction)resetCrop:(id)sender {
    if(sourceIsCamera){
        UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil);
    }
    [self.croppingBackground removeFromSuperview];
    self.frontCardView.alpha = 1;
    self.backCardView.alpha = 1;
    self.nameButton.alpha = 1;
    self.categoryButton.alpha = 1;
    self.doneButton.alpha = 1;
    self.exitCropButton.hidden = TRUE;
    CGSize imageSize = self.image.size;
    if(imageSize.height > 129){
        
        double ratio = 129/imageSize.height;
        imageSize.height = 129;
        imageSize.width = imageSize.width*ratio;
    }
    
    if(imageSize.width > 266){
        
        double ratio = 266/imageSize.width;
        imageSize.width = 266;
        imageSize.height = imageSize.height*ratio;
    }
    
    
    if (self.image != nil)
    {
        NSLog(@"picture loaded");
        //        [self loadCardImage:selectedImage :YES];
        
        if (buttonLabel)
        {
            
            self.frontCardImageView.hidden = NO;
            self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangleEmpty.png"]];
            //if the label is already there hide it
            if (!isFrontImage)
            {
                self.frontCardLabel.hidden = true;
                self.frontCardImageView.hidden = false;
            }
            //set the flag for image 
            isFrontImage = true; 
            //set the image
            self.frontCardImageView.image = self.image;
            self.frontCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                       , (137-imageSize.height)/2, imageSize.width, imageSize.height);
            isFrontCardLoaded = true;
            isFrontImage = true;
        }
        else
        {
            self.backCardImageView.hidden = NO;
            self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangleEmpty.png"]];
            //if the label is already there hide it
            if (!isBackImage)
            {
                self.backCardLabel.hidden = true;
                self.backCardImageView.hidden = false;
            }   
            //set the flag for image 
            isBackImage = true; 
            //set the image
            self.backCardImageView.image = self.image;
            
            self.backCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                      , (137-imageSize.height)/2, imageSize.width, imageSize.height);
            isBackCardLoaded = true;
            isBackImage = true;
        }
        
        
        
    }
    self.front = true;
    self.frontBackButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frontEnabledBackground.png"]];

    
}

- (IBAction)cropImage:(id)sender {
    NSString *label;
    
    if(sourceIsCamera){
        UIImageWriteToSavedPhotosAlbum(self.image, nil, nil, nil);
        label = @"camera";
    }
    else {
        label = @"library";
    }
    
    //track event for editing deck name
    NSError *error;
    if (![[GANTracker sharedTracker] trackEvent:@"createcard"
                                         action:@"crop"
                                          label:label
                                          value:-1
                                      withError:&error]) {
        NSLog(@"Error: %@", error);
    }
    
    UIImage *backImage = [self.cropViewBack cropImage:self.image];
    UIImage *frontImage = [self.cropViewFront cropImage:self.image];
    if(self.cropViewFront.frame.origin.x > self.cropViewBack.frame.origin.x && self.cropViewFront.frame.origin.y > self.cropViewBack.frame.origin.y){
        CGRect coverFrame = CGRectMake(self.cropViewFront.frame.origin.x-self.cropViewBack.frame.origin.x, self.cropViewFront.frame.origin.y-self.cropViewBack.frame.origin.y, self.cropViewFront.frame.size.width, self.cropViewFront.frame.size.height);
        backImage = [self.cropViewBack cropImageAndBlackOut:self.image withFrame:coverFrame];
    }
    [self.croppingBackground removeFromSuperview];
    self.frontCardView.alpha = 1;
    self.backCardView.alpha = 1;
    self.nameButton.alpha = 1;
    self.categoryButton.alpha = 1;
    self.doneButton.alpha = 1;
    self.exitCropButton.hidden = TRUE;
    CGSize imageSize = frontImage.size;
    if(imageSize.height > 129){
        
        double ratio = 129/imageSize.height;
        imageSize.height = 129;
        imageSize.width = imageSize.width*ratio;
    }
    if(imageSize.width > 266){
        
        double ratio = 266/imageSize.width;
        imageSize.width = 266;
        imageSize.height = imageSize.height*ratio;
    }
    
    if(self.front){
        if (frontImage != nil)
        {
            NSLog(@"picture loaded");
            //        [self loadCardImage:selectedImage :YES];
            
            if (buttonLabel)
            {
                self.frontCardImageView.hidden = NO;
                self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangleEmpty.png"]];
                //if the label is already there hide it
                if (!isFrontImage)
                {
                    self.frontCardLabel.hidden = true;
                    self.frontCardImageView.hidden = false;
                }
                //set the flag for image 
                isFrontImage = true; 
                //set the image
                self.frontCardImageView.image = frontImage;
                self.frontCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                           , (137-imageSize.height)/2, imageSize.width, imageSize.height);
                isFrontCardLoaded = true;
                isFrontImage = true;
            }
            else
            {
                self.backCardImageView.hidden = NO;
                
                self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangleEmpty.png"]];
                //if the label is already there hide it
                if (!isBackImage)
                {
                    self.backCardLabel.hidden = true;
                    self.backCardImageView.hidden = false;
                }   
                //set the flag for image 
                isBackImage = true; 
                //set the image
                self.backCardImageView.image = frontImage;
                
                self.backCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                          , (137-imageSize.height)/2, imageSize.width, imageSize.height);
                isBackCardLoaded = true;
                isBackImage = true;
            }        
        }
    }
    else{
        
        //Add the front image
        self.frontCardImageView.hidden = NO;
        self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangleEmpty.png"]];
        //if the label is already there hide it
        if (!isFrontImage)
        {
            self.frontCardLabel.hidden = true;
            self.frontCardImageView.hidden = false;
        }
        //set the flag for image 
        isFrontImage = true; 
        //set the image
        self.frontCardImageView.image = frontImage;
        self.frontCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                   , (137-imageSize.height)/2, imageSize.width, imageSize.height);
        isFrontCardLoaded = true;
        isFrontImage = true;
        
        //Reset image size for the back image and add it
        imageSize = backImage.size;
        
        if(imageSize.height > 129){
            
            double ratio = 129/imageSize.height;
            imageSize.height = 129;
            imageSize.width = imageSize.width*ratio;
        }
        if(imageSize.width > 266){
            
            double ratio = 266/imageSize.width;
            imageSize.width = 266;
            imageSize.height = imageSize.height*ratio;
        }

        self.backCardImageView.hidden = NO;
                self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangleEmpty.png"]];
        //if the label is already there hide it
        if (!isBackImage)
        {
            self.backCardLabel.hidden = true;
            self.backCardImageView.hidden = false;
        }   
        //set the flag for image 
        isBackImage = true; 
        //set the image
        self.backCardImageView.image = backImage;
        
        self.backCardImageView.frame = CGRectMake((272-imageSize.width)/2
                                                  , (137-imageSize.height)/2, imageSize.width, imageSize.height);
        isBackCardLoaded = true;
        isBackImage = true;
    }
    self.front = true;
    self.frontBackButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frontEnabledBackground.png"]];
}

- (IBAction)frontBackButtonPressed:(id)sender {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(myTransitionDidStop:)];
    
    // swap the views and transition
    if (self.front) {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.frontBackButton cache:YES];
        
        self.frontBackButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frontBackEnabledBackground.png"]];
        
        
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.frontBackButton cache:YES];
        self.frontBackButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"frontEnabledBackground.png"]];
        
    }
    [UIView commitAnimations];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(myTransitionDidStop:)];
    
    [UIView commitAnimations];
    if(self.front){
        self.cropViewBack.hidden = false;
        self.frontCropLabel.text = @"FRONT";
    }
    else{
        self.cropViewBack.hidden = true;
        self.frontCropLabel.text = nil;
    }
    self.front = !self.front;
}

- (IBAction)helpPressed:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.helpPop.alpha = 1;
    [UIView commitAnimations];
}

- (IBAction)closeAd:(id)sender {
    self.closeAdButton.alpha=0;
    _bannerView.hidden=true;
}


//Methods for handling clicks on the front and back of the card
- (void)handleClickOnFront:(UITapGestureRecognizer *)recognizer { 
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    buttonLabel = true;
    choosePopUp.alpha = 1;
    [UIView commitAnimations];
    
}


- (void)handleClickOnBack:(UITapGestureRecognizer *)recognizer {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    buttonLabel = false;
    choosePopUp.alpha = 1;
    [UIView commitAnimations];
    
}

//Delegate for FCPopUp
- (void) FCPopUpFinished: (int)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            NSLog(@"Cancel");
            break;
            // PopUp for text
        case 1:
            NSLog(@"Text");
            if([self.mode isEqualToString:@"Edit"]){
                enterText.content.frame = CGRectMake(25,10, 270, 220);
                enterText.closeButton.frame = CGRectMake(278, 0, 28, 28);
                
            }
            //popOver for entering text
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            
            enterText.alpha = 1;
            [UIView commitAnimations];
            break;
            //Open Camera    
        case 2:
            NSLog(@"Camera");
            isImagePickerCalled = true;
            sourceIsCamera = true;
            self.picker = nil;
            if (self.picker == nil) {   
                self.picker = [[UIImagePickerController alloc] init];
                self.picker.delegate = self;
                self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                self.picker.allowsEditing = YES;   
                
            } 
            [self.tabBarController presentModalViewController:self.picker animated:YES];
            break;
        case 3:
            NSLog(@"Library");
            isImagePickerCalled = true;
            sourceIsCamera = false;
            self.picker = nil;
            if (self.picker == nil) {   
                self.picker = [[UIImagePickerController alloc] init];
                self.picker.delegate = self;
                self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                self.picker.allowsEditing = YES;
                
            } 
            [self.tabBarController presentModalViewController:self.picker animated:YES];
            break;
        default:
            break;
    }
}

- (void) enterTextFinished: (NSString *)textExtract{    
    [self.view endEditing:YES];
    if (buttonLabel){
        self.frontCardImageView.hidden = YES;
        self.frontCardLabel.hidden = NO;
        //if the imageView is loaded then hide it
        if (isFrontImage)
        {
            self.frontCardImageView.hidden = true;
            self.frontCardLabel.hidden = false;                    
        }
        isFrontImage = false;
        self.frontCardLabel.text = textExtract;
        isFrontCardLoaded = true;
        isFrontImage = false;
        self.frontCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsTopRectangleEmpty.png"]];
    }
    else {
        
        self.backCardImageView.hidden = YES;
        self.backCardLabel.hidden = NO;
        self.backCardView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"createCardsBottomRectangleEmpty.png"]];
        //if the imageView is loaded then hide it
        if (isBackImage)
        {
            self.backCardImageView.hidden = true;
            self.backCardLabel.hidden = false;
        }
        isBackImage = false;
        self.backCardLabel.text = textExtract;
        isBackCardLoaded = true;
        isBackImage = false;
    }
    self.enterText.textBox.text = @"";
    
}

//delegate for category
- (void) categoryFinished: (NSString *)textExtract{
    
    textExtract = [textExtract uppercaseString];
    if ([categoryMode isEqualToString:@"Deck"])
        self.nameButton.titleLabel.text = textExtract;
    else {
        self.categoryButton.titleLabel.text = textExtract;
    }
    
}

//delegate for imagePicker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)selectedImage editingInfo:(NSDictionary *)editingInfo {
    self.frontCardView.alpha = 0;
    self.backCardView.alpha = 0;
    self.nameButton.alpha = 0;
    self.categoryButton.alpha = 0;
    self.doneButton.alpha = 0;
    self.exitCropButton.hidden = FALSE;
    
    
    self.image = selectedImage;
    CGSize newSize;
    if(isRetina){
        NSLog(@"RETINA DETECTED");
        newSize = CGSizeMake(640, 640);
    }
    else{
        newSize = CGSizeMake(320, 320);
    }
    UIGraphicsBeginImageContext(newSize);
    [self.image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    self.image = newImage;
    
    self.frontBackButton.hidden = false;
    self.croppingBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.croppingBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    
    UIImageView * selectedImageView = [[UIImageView alloc] initWithImage:self.image];
    
    selectedImageView.frame = CGRectMake(0, 45, 320, 320);
    if([self.mode isEqualToString:@"Edit"]){
        selectedImageView.frame = CGRectMake(0, 0, 320, 320);
        

    }
    UILabel * croppingTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    croppingTitle.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NavBackground.png"]];
    croppingTitle.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:41];
    croppingTitle.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    croppingTitle.textAlignment = UITextAlignmentCenter;
    croppingTitle.textColor =[UIColor whiteColor];
    croppingTitle.shadowColor = [UIColor colorWithWhite:0 alpha:1];
    croppingTitle.shadowOffset = CGSizeMake(1, 1);
    croppingTitle.text = @"CROP";
    
    
    self.cropButton.alpha = 1;
    self.resetButton.alpha = 1;
    
    CGRect puttyFrame = CGRectMake(10, 10, 300, 100);
    self.cropViewFront= [[cropperView alloc] initWithFrame:puttyFrame];
    selectedImageView.userInteractionEnabled = YES;
    [selectedImageView addSubview:self.cropViewFront];
    
    self.frontCropLabel = [[UILabel alloc] init];
    self.frontCropLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:41];
    self.frontCropLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    self.frontCropLabel.textAlignment = UITextAlignmentCenter;
    self.frontCropLabel.textColor =[UIColor colorWithWhite:1 alpha:0.25];
    if(!self.front){
        self.frontCropLabel.text = @"FRONT";
    }
    else{
        self.frontCropLabel.text = nil;
    }
    self.cropViewFront.contentView = self.frontCropLabel;
    self.cropViewFront.rectangle.layer.borderColor = [UIColor colorWithRed:0 green:1 blue:0.07 alpha:1].CGColor;
    
    CGRect puttyFrame2 = CGRectMake(10, 115, 300, 100);
    self.cropViewBack= [[cropperView alloc] initWithFrame:puttyFrame2];
    selectedImageView.userInteractionEnabled = YES;
    if(self.front){
        self.cropViewBack.hidden = true;
    }
    [selectedImageView addSubview:self.cropViewBack];
    
    self.backCropLabel = [[UILabel alloc] init];
    
    self.backCropLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:41];
    self.backCropLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    self.backCropLabel.textAlignment = UITextAlignmentCenter;
    self.backCropLabel.textColor =[UIColor colorWithWhite:1 alpha:0.25];
    self.backCropLabel.text = @"BACK";
    self.cropViewBack.contentView = self.backCropLabel;
    
    self.cropViewBack.rectangle.layer.borderColor = [UIColor colorWithRed:1 green:1 blue:0 alpha:1].CGColor;
    [self.view addSubview:self.croppingBackground];
    [self.view bringSubviewToFront:self.croppingBackground];
    [self.croppingBackground addSubview:self.cropButton];
    [self.croppingBackground addSubview:self.resetButton];
    [self.croppingBackground addSubview:self.exitCropButton];
    if(![self.mode isEqualToString:@"Edit"]){
        [self.croppingBackground addSubview:croppingTitle];
    }
    [self.croppingBackground addSubview:self.frontBackButton];
    [self.croppingBackground addSubview:selectedImageView];
    if([self.mode isEqualToString:@"Edit"]){
        self.cropButton.frame = CGRectMake(179, 322, 128, 27);
        self.resetButton.frame = CGRectMake(13, 322, 128, 27);
    }
    [self.croppingBackground bringSubviewToFront:selectedImageView];
    if(![self.mode isEqualToString:@"Edit"]){
        [self.croppingBackground bringSubviewToFront:self.frontBackButton];
        [self.croppingBackground bringSubviewToFront:self.exitCropButton];
    }
    else{
        self.exitCropButton.hidden = TRUE;
    }
    [selectedImageView bringSubviewToFront:self.cropViewFront];
    
    
    [self.picker dismissModalViewControllerAnimated:YES];
    
    
    
}

- (IBAction)exitCropPressed:(id)sender{
    [self.croppingBackground removeFromSuperview];
    self.frontCardView.alpha = 1;
    self.backCardView.alpha = 1;
    self.nameButton.alpha = 1;
    self.categoryButton.alpha = 1;
    self.doneButton.alpha = 1;
    self.exitCropButton.hidden = TRUE;
    
}




- (void)layoutAnimated:(BOOL)animated
{
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    
    
    CGRect bannerFrame = _bannerView.frame;
    bannerFrame = CGRectMake(0, 360, 320, 50);
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _bannerView.frame = bannerFrame;
    }];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
    self.closeAdButton.alpha=1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Ad failed to load!");
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}





@end
