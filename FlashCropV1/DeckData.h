//
//  DeckData.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CardData, ResultData, UserData;

@interface DeckData : NSManagedObject

@property (nonatomic, retain) NSString * deckName;
@property (nonatomic, retain) NSNumber * deckNumber;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) UserData *toUser;
@property (nonatomic, retain) NSSet *toCard;
@property (nonatomic, retain) NSSet *toResult;
@end

@interface DeckData (CoreDataGeneratedAccessors)

- (void)addToCardObject:(CardData *)value;
- (void)removeToCardObject:(CardData *)value;
- (void)addToCard:(NSSet *)values;
- (void)removeToCard:(NSSet *)values;

- (void)addToResultObject:(ResultData *)value;
- (void)removeToResultObject:(ResultData *)value;
- (void)addToResult:(NSSet *)values;
- (void)removeToResult:(NSSet *)values;

@end
