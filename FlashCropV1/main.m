//
//  main.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "fcv1AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([fcv1AppDelegate class]));
    }
}
