//
//  fcv1AppDelegate.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "coreServices.h"
#import "GANTracker.h"
#import "Flurry.h"

@interface fcv1AppDelegate : NSObject <UIApplicationDelegate, GANTrackerDelegate>{
    
    UIWindow *window;
    UINavigationController *navigationController;
	
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;	    
}


@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (NSString *)applicationDocumentsDirectory;
- (void)saveContext;


@end
