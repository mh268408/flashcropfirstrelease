//
//  FCTableViewController.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 8/7/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"

@interface FCTableViewController : FCUIView <UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIButton *editButton;
@end