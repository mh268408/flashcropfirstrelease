//
//  cropperView.m
//  FlashCrop
//
//  Created by  on 7/9/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "cropperView.h"
#import <QuartzCore/QuartzCore.h>
#define kResizeThumbSize 40

@implementation cropperView

@synthesize contentView;
@synthesize rectangle;
@synthesize topRightCorner;
@synthesize bottomRightCorner;
@synthesize bottomLeftCorner;

bool isResizingLR, isResizingUL, isResizingUR, isResizingLL, isRetina;



-(UIImage *) cropImage:(UIImage *) image{
    isRetina = ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?1:0;
    CGRect rect;
    if(isRetina){
        
        rect = CGRectMake((self.frame.origin.x+10)*2, (self.frame.origin.y+10)*2,
                          (self.frame.size.width-20)*2, (self.frame.size.height-20)*2);
    }
    else{
        
        rect = CGRectMake(self.frame.origin.x+10, self.frame.origin.y+10,
                          self.frame.size.width-20, self.frame.size.height-20);
    }
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    return img;
}

-(UIImage *) cropImageAndBlackOut:(UIImage *) image withFrame:(CGRect) coverFrame{
    isRetina = ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?1:0;
    CGRect rect;
    if(isRetina){
        
        rect = CGRectMake((self.frame.origin.x+10)*2, (self.frame.origin.y+10)*2,
                          (self.frame.size.width-20)*2, (self.frame.size.height-20)*2);
    }
    else{
        
        rect = CGRectMake(self.frame.origin.x+10, self.frame.origin.y+10,
                          self.frame.size.width-20, self.frame.size.height-20);
    }
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    UIImage *cover = [UIImage imageNamed:@"white.png"];
    CGRect myRect = coverFrame;
    UIGraphicsBeginImageContext(myRect.size);
    [cover drawInRect:CGRectMake(0, 0, myRect.size.width, myRect.size.height)];
    UIImage *coverFull = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIGraphicsBeginImageContext(img.size);
    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    if(isRetina){
        [coverFull drawInRect:CGRectMake(myRect.origin.x*2, myRect.origin.y*2, (coverFull.size.width-22)*2, (coverFull.size.height-22)*2)];
    }
    else{
        [coverFull drawInRect:CGRectMake(myRect.origin.x, myRect.origin.y, coverFull.size.width-20, coverFull.size.height-20)];
    }
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finalImage;
}



- (void)setContentView:(UIView *)newContentView {
    [contentView removeFromSuperview];
    contentView = newContentView;
    contentView.frame = self.bounds;
    contentView.backgroundColor = [UIColor clearColor];
    rectangle = [[UIView alloc] initWithFrame:CGRectMake(contentView.frame.origin.x+10, contentView.frame.origin.y+10, contentView.frame.size.width-20, contentView.frame.size.height-20)];
    rectangle.layer.borderColor = [UIColor blackColor].CGColor;
    rectangle.layer.borderWidth = 2;
    
    UIImageView *topLeftCorner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"croppingCorner.png"]];
    topLeftCorner.frame = CGRectMake(0, 0, 30, 30);
    topRightCorner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"croppingCorner.png"]];
    topRightCorner.frame = CGRectMake(self.frame.size.width-30, 0, 30, 30);
    bottomLeftCorner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"croppingCorner.png"]];
    bottomLeftCorner.frame = CGRectMake(0, self.frame.size.height-30, 30, 30);
    bottomRightCorner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"croppingCorner.png"]];
    bottomRightCorner.frame = CGRectMake( self.frame.size.width-30, self.frame.size.height-30, 30, 30);
    
    
    
    self.rectangle.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.rectangle.layer.shadowOffset = CGSizeMake(3, 3);
    self.rectangle.layer.shadowRadius = 5;
    self.rectangle.layer.shadowOpacity = 0.8;
    
    [self addSubview:rectangle];
    [self addSubview:topLeftCorner];
    [self addSubview:topRightCorner];
    [self addSubview:bottomLeftCorner];
    [self addSubview:bottomRightCorner];
    [self bringSubviewToFront:topLeftCorner];
    [self bringSubviewToFront:topRightCorner];
    [self bringSubviewToFront:bottomLeftCorner];
    [self bringSubviewToFront:bottomRightCorner];
    [self addSubview:contentView];
}

- (void)setFrame:(CGRect)newFrame {
    [super setFrame:newFrame];
    contentView.frame = self.bounds;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    touchStart = [[touches anyObject] locationInView:self];
    isResizingLR = (self.bounds.size.width - touchStart.x < kResizeThumbSize && self.bounds.size.height - touchStart.y < kResizeThumbSize);
    isResizingUL = (touchStart.x <kResizeThumbSize && touchStart.y <kResizeThumbSize);
    isResizingUR = (self.bounds.size.width-touchStart.x < kResizeThumbSize && touchStart.y<kResizeThumbSize);
    isResizingLL = (touchStart.x <kResizeThumbSize && self.bounds.size.height -touchStart.y <kResizeThumbSize);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    CGPoint previous=[[touches anyObject]previousLocationInView:self];
    
    float  deltaWidth = touchPoint.x-previous.x;
    float  deltaHeight = touchPoint.y-previous.y;
    
    if (isResizingLR ) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, touchPoint.x + deltaWidth, touchPoint.y + deltaWidth);
    }  
    if (isResizingUL) {
        self.frame = CGRectMake(self.frame.origin.x + deltaWidth, self.frame.origin.y + deltaHeight, self.frame.size.width - deltaWidth, self.frame.size.height - deltaHeight);
    } 
    if (isResizingUR) {
        self.frame = CGRectMake(self.frame.origin.x ,self.frame.origin.y + deltaHeight,  self.frame.size.width + deltaWidth, self.frame.size.height - deltaHeight);      
    } 
    if (isResizingLL) {
        self.frame = CGRectMake(self.frame.origin.x + deltaWidth ,self.frame.origin.y ,  self.frame.size.width - deltaWidth, self.frame.size.height + deltaHeight);   
    }
    
    if (!isResizingUL && !isResizingLR && !isResizingUR && !isResizingLL) {
        self.center = CGPointMake(self.center.x + touchPoint.x - touchStart.x,self.center.y + touchPoint.y - touchStart.y);
    }
    
    if(self.contentView.frame.size.height < 30){
        self.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 30);
    }
    if(self.contentView.frame.size.width < 30){
        self.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, 30, self.contentView.frame.size.height);
    }
    
    
    self.rectangle.frame = CGRectMake(self.contentView.frame.origin.x+10, self.contentView.frame.origin.y+10, self.contentView.frame.size.width-20, self.contentView.frame.size.height-20);
    self.topRightCorner.frame = CGRectMake(self.contentView.frame.size.width-30, 0, 30, 30);
    self.bottomLeftCorner.frame = CGRectMake(0, self.contentView.frame.size.height-30, 30, 30);
    self.bottomRightCorner.frame = CGRectMake(self.contentView.frame.size.width-30, self.contentView.frame.size.height-30, 30, 30);
    
} 



@end

