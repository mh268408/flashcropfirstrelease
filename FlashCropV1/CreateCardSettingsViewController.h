//
//  CreateCardSettingsViewController.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"
#import "coreServices.h"


@interface CreateCardSettingsViewController : FCUIView<UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *autoNameCheckBox;
@property (strong, nonatomic) IBOutlet UITextField *namePrefixBox;
@property (strong, nonatomic) coreServices *coreData;
@property UIColor * grey;
@property UIColor * green;
@property UIColor * orange;

- (IBAction)autoNameCheckBoxPressed:(id)sender;

@end
