//
//  fcv1SecondViewController.h
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "FCUIView.h"
#import "coreServices.h"
#import "CreateCardsViewController.h"
#import "alertPopUp.h"

@interface ViewCardsViewController : FCUIView<UINavigationControllerDelegate, ADBannerViewDelegate>
@property (nonatomic, strong)  IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *cardBackground;
@property (strong, nonatomic) IBOutlet UIImageView *nextCardArrow;
@property (strong, nonatomic) IBOutlet UIImageView *previousCardArrow;
@property (strong, nonatomic) IBOutlet UIView *frontView;
@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UIButton *correctButton;
@property (strong, nonatomic) IBOutlet UIButton *incorrectButton;
@property (strong, nonatomic) UITapGestureRecognizer *pinchScreen;
@property (strong, nonatomic) IBOutlet UIImageView *dummyCardBackground;
@property (weak, nonatomic) IBOutlet UILabel *backLabel;
@property (weak, nonatomic) IBOutlet UILabel *frontLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIImageView *frontImage;
@property (strong, nonatomic) IBOutlet UIImageView *correctPopUp;
@property (strong, nonatomic) IBOutlet UIImageView *incorrectPopUp;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) IBOutlet UIButton *exitFullscreenButton;
@property (strong, nonatomic) alertPopUp *alertPop;

@property (strong, nonatomic) DeckData *deck;
@property (strong, nonatomic) CardData *card;
@property (weak, nonatomic) IBOutlet UIButton *closeAdButton;

- (IBAction)screenTapped:(id)sender;
- (IBAction)correctButtonPressed:(id)sender;
- (IBAction)incorrectButtonPressed:(id)sender;
- (IBAction)favoriteButtonPushed:(UIButton *)sender;
- (IBAction)deleteCard:(UIButton *)sender;
- (IBAction)pinchDetected:(id)sender;
- (IBAction)closeAd:(id)sender;



@end
