//
//  ImageToData.m
//  FlashCrop
//
//  Created by Arkopaul Sarkar on 7/10/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "ImageToData.h"

@implementation ImageToData
+ (BOOL)allowsReverseTransformation {
	return YES;
}

+ (Class)transformedValueClass {
	return [NSData class];
}

- (id)transformedValue:(id)value {
	return UIImagePNGRepresentation(value);
}

- (id)reverseTransformedValue:(id)value {
	return [[UIImage alloc] initWithData:value];
}

@end
