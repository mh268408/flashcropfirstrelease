//
//  fcv1FirstViewController.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "HomeViewController.h"
#import "PieChartView.h"

@implementation HomeViewController
@synthesize pieChart = _pieChart;
@synthesize userPicture;
@synthesize welcomePopOver;
@synthesize nameBox;
@synthesize emailBox;
@synthesize profilePicture;
@synthesize daysSince;
@synthesize correctLabel;
@synthesize incorrectLabel;
@synthesize daySinceText;
@synthesize backgroundView; 
@synthesize containerView;
@synthesize usernameBorder;
@synthesize greetingMessage;
@synthesize userName;
@synthesize separator;
@synthesize welcomeContentView;
@synthesize helpPop;


UIImagePickerController * picker;


- (void)viewDidLoad
{
    self.navBar = false;
    [super viewDidLoad];
    self.welcomeContentView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    self.welcomePopOver.hidden = true;
    //Load core data 
    //load the core service
    bool isUserPresent = [coreServices loadData];
    if (!isUserPresent){
        //Need to have a first time user registration screen here
        // temporarily registers with system name
        // get userName and emailID and userPicture from the screen and 
        self.welcomePopOver.hidden = false;
        self.welcomePopOver.alpha = 1;
        self.tabBarController.tabBar.hidden = YES;

    }
    self.nameBox.delegate = self;
    self.emailBox.delegate = self;
    
    self.usernameBorder.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeUsernameBorder.png"]];
    self.separator.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeVerticalDivider.png"]];
    self.greetingMessage.backgroundColor = NULL;   
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"helpView" owner:self options:nil];
    self.helpPop = [subviewArray objectAtIndex:0];
    self.helpPop.fileName = @"HOMEHELP";
    [self.helpPop style];
    self.helpPop.alpha = 0;
    [self.view addSubview:self.helpPop];

    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSNumber *correct;
    NSNumber *incorrect;
    // Do any additional setup after loading the view, typically from a nib.
    if ([[coreServices getUserData] count] > 0){
        self.userName.text = [[[coreServices getUserData] objectAtIndex:0] userName];
        self.userPicture.image = [[[coreServices getUserData] objectAtIndex:0] userPicture];
    }
    
    PieChartView *result = [_pieChart initWithFrame:CGRectMake(0, 0, 100, 100)];
    if(result != nil){
        NSLog(@"Table view loaded successfully");
    }
    
    NSArray *allResult = [[NSMutableArray alloc] init];
    //Get data for pie chart
    //get result for last 10 results
    for (DeckData *deck in [coreServices getDeckData]) {
        allResult = [allResult arrayByAddingObjectsFromArray:deck.toResult.allObjects];
    }
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:false];
    NSMutableArray *allResultMutable = [allResult mutableCopy];
    [allResultMutable sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    if (allResultMutable.count > 0) {
        NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:[[allResultMutable objectAtIndex:0] timeStamp]];
        NSLog(@"%@", [NSString stringWithFormat:@"%.f", floor(secondsBetween / 86400)]);
        self.daysSince.text = [NSString stringWithFormat:@"%.f", floor(secondsBetween / 86400)];
        if (floor(secondsBetween / 86400) < 2) {
            self.daySinceText.text = [NSString stringWithFormat:@"%@", @"DAY SINCE"];
        }
        
        int resultCount = 0;
        if (allResultMutable.count > 10) {
            resultCount = 10;
        }
        else {
            resultCount = allResultMutable.count;
        }
        
        //get number of correct and incorrect
        for (int i = 0; i< resultCount; i++) {
            correct = [NSNumber numberWithFloat:[[[allResultMutable objectAtIndex:i] right] floatValue]+ [correct floatValue]];
            incorrect = [NSNumber numberWithFloat:[[[allResultMutable objectAtIndex:i] wrong] floatValue]+ [incorrect floatValue]];
        }
        
        self.correctLabel.text = [NSString stringWithFormat:@"%.1f", correct.floatValue*100/(correct.floatValue+incorrect.floatValue)];
        self.incorrectLabel.text = [NSString stringWithFormat:@"%.1f", incorrect.floatValue*100/(correct.floatValue+incorrect.floatValue)];
    }
    else {
        self.daysSince.text = [NSString stringWithFormat:@"%@", @"0"];
        
        self.daySinceText.text = [NSString stringWithFormat:@"%@", @"DAY SINCE"];
        correct = [NSNumber numberWithFloat:0.0];
        incorrect = [NSNumber numberWithFloat:0.0];
        self.correctLabel.text = [NSString stringWithFormat:@"%.1d", 0];
        self.incorrectLabel.text = [NSString stringWithFormat:@"%.1d", 0];
    }
    
    
	[_pieChart clearItems];
	
	[_pieChart setGradientFillStart:0.3 andEnd:1.0];
	[_pieChart setGradientFillColor:PieChartItemColorMake(0.0, 0.0, 0.0, 0.7)];
	
	[_pieChart addItemValue:correct.floatValue/(correct.floatValue+incorrect.floatValue) withColor:PieChartItemColorMake(0.18, 0.533, 0.227, 1)];
	[_pieChart addItemValue:incorrect.floatValue/(correct.floatValue+incorrect.floatValue) withColor:PieChartItemColorMake(0.9, 0.5, 0.2, 1)];
	
    
	_pieChart.alpha = 0.0;
	[_pieChart setHidden:NO];
	[_pieChart setNeedsDisplay];
	
	// Animate the fade-in
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
	_pieChart.alpha = 1.0;
	[UIView commitAnimations];
    
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarHome.png"]]  atIndex:0]; 
        
}


- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setUsernameBorder:nil];
    [self setGreetingMessage:nil];
    [self setUserName:nil];
    [self setSeparator:nil];
    [self setPieChart:nil];
    [self setUserPicture:nil];
    [self setWelcomePopOver:nil];
    [self setNameBox:nil];
    [self setEmailBox:nil];
    [self setProfilePicture:nil];
    [self setDaysSince:nil];
    [self setCorrectLabel:nil];
    [self setIncorrectLabel:nil];
    [self setDaySinceText:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


- (IBAction)choosePictureButtonPressed:(id)sender {
    picker.delegate = self;
    picker = nil;
    if (picker == nil) {   
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
        
    } 
    [self.tabBarController presentModalViewController:picker animated:YES];    
}

- (IBAction)doneButtonPressed:(id)sender {
    //create user with the data  
    [coreServices createUser:self.nameBox.text :self.emailBox.text :self.profilePicture.image];
    
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    //also load some default settings 
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 positive, @"autoDeckname", 
                                 @"DECK", @"cardNamePrefix",  
                                 negative, @"prioritizeFavorite",  
                                 negative, @"prioritizeWrongCards", 
                                 negative, @"statInNumber", 
                                 negative, @"studyRepeat", 
                                 negative, @"studyShuffle", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:
                                positive, @"autoDeckname", 
                                positive, @"cardNamePrefix",  
                                positive, @"prioritizeFavorite",  
                                positive, @"prioritizeWrongCards", 
                                positive, @"statInNumber", 
                                positive, @"studyRepeat", 
                                positive, @"studyShuffle", 
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    
    // display username and picture
    self.userName.text = [[[coreServices getUserData] objectAtIndex:0] userName];
    self.userPicture.image = [[[coreServices getUserData] objectAtIndex:0] userPicture];
    [self.nameBox resignFirstResponder];
    [self.emailBox resignFirstResponder];
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.welcomePopOver.alpha = 0;
    self.tabBarController.tabBar.hidden = NO;
    [UIView commitAnimations];
    
}

- (IBAction)skipButtonPressed:(id)sender {
    
    [coreServices createUser:@"Guest User" :@"email@internet.com" :[UIImage imageNamed:@"defaultProfilePicture.png"]];
    
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    //also load some default settings 
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 positive, @"autoDeckname", 
                                 @"DECK", @"cardNamePrefix",  
                                 negative, @"prioritizeFavorite",  
                                 negative, @"prioritizeWrongCards", 
                                 negative, @"statInNumber", 
                                 negative, @"studyRepeat", 
                                 negative, @"studyShuffle", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:
                                positive, @"autoDeckname", 
                                positive, @"cardNamePrefix",  
                                positive, @"prioritizeFavorite",  
                                positive, @"prioritizeWrongCards", 
                                positive, @"statInNumber", 
                                positive, @"studyRepeat", 
                                positive, @"studyShuffle", 
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    // display username and picture
    self.userName.text = [[[coreServices getUserData] objectAtIndex:0] userName];
    self.userPicture.image = [[[coreServices getUserData] objectAtIndex:0] userPicture];
    self.tabBarController.tabBar.hidden = NO;
    self.welcomePopOver.hidden = true;
}

- (IBAction)helpPressed:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.helpPop.alpha = 1;
    [UIView commitAnimations];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)selectedImage editingInfo:(NSDictionary *)editingInfo {
    self.profilePicture.image = selectedImage;
    [picker dismissModalViewControllerAnimated:YES];
    
}
@end
