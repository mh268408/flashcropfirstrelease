//
//  AccountSettingsViewController.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"
#import "coreServices.h"


@interface AccountSettingsViewController : FCUIView <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UITextField *nameBox;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
- (IBAction)changePicturePressed:(id)sender;
- (IBAction)createAccountPressed:(id)sender;

@end
