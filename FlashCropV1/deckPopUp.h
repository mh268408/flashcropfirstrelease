//
//  deckPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/16/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "coreServices.h"

@protocol DeckPopDelegate <NSObject>
@required
- (void) deckPopFinished: (NSString *)deckName :(NSString *)category;
@end

@interface deckPopUp : UIView <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
	id <DeckPopDelegate> delegate;
}
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UIView *content;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UILabel *title1;
@property (strong, nonatomic) IBOutlet UILabel *title2;
@property (strong, nonatomic) IBOutlet UITextField *changeDeckTextField;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (retain) id delegate;
- (IBAction)closePopUp:(id)sender;
-(void) style;
- (IBAction)button1Pressed:(id)sender;

@end
