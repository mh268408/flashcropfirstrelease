//
//  FCTableViewController.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 8/7/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCTableViewController.h"

@interface FCTableViewController ()

@end

@implementation FCTableViewController
@synthesize tableView;
@synthesize editButton;
BOOL inEdit = false;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navBar = true;
    self.title = @"MY CARDS";
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tableDivider.png"]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.editButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 54, 30)];
    [self.editButton setImage:[UIImage imageNamed:@"editButton.png"] forState:UIControlStateNormal];
    [self.editButton setShowsTouchWhenHighlighted:TRUE];
    [self.editButton addTarget:self action:@selector(editButtonPushed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:self.editButton];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.rightBarButtonItem = barBackItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  

    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarMyCards.png"]]  atIndex:0]; 
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setTableView:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) editButtonPushed{
    
    if (!inEdit) {
        
        UIButton * doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 54, 30)];
        [doneButton setImage:[UIImage imageNamed:@"doneButton.png"] forState:UIControlStateNormal];
        [doneButton setShowsTouchWhenHighlighted:TRUE];
        [doneButton addTarget:self action:@selector(editButtonPushed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.rightBarButtonItem = barBackItem;
        
        
        inEdit = true;
        [self.tableView setEditing:YES animated:YES];
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationFade];
        ;
        
    }
    else {
        
        self.editButton.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"editButton.png"]];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:self.editButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.rightBarButtonItem = barBackItem;
        inEdit = false; 
        [self.tableView setEditing:NO animated:YES];
        [self.tableView reloadRowsAtIndexPaths:[self.tableView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationFade];
        ;
    }
}

//delegates for table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 51;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    return cell;
}

@end
