//
//  CreateCardsViewController.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "FCUIView.h"
#import "coreServices.h"
#import "FCPopUp.h"
#import "namePopUp.h"
#import "enterTextPopUp.h"
#import "categoryPopUp.h"
#import "alertPopUp.h"
#import "cropperView.h"
#import "ViewCardsViewController.h"
#import "helpPopUp.h"


@interface CreateCardsViewController : FCUIView <UIImagePickerControllerDelegate,FCPopUpDelegate, enterTextDelegate, CategoryDelegate, UINavigationControllerDelegate, ADBannerViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *nameButton;
@property (strong, nonatomic) IBOutlet UIButton *categoryButton;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UILabel *topCardNumber;

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (strong, nonatomic) UIImagePickerController * picker;
@property (strong, nonatomic) UIImage *image;

@property (weak, nonatomic) IBOutlet UIView *frontCardView;
@property (nonatomic, retain) IBOutlet UIImageView *frontCardImageView;
@property (weak, nonatomic) IBOutlet UILabel *frontCardLabel;

@property (weak, nonatomic) IBOutlet UIView *backCardView;
@property (nonatomic, retain) IBOutlet UIImageView *backCardImageView;
@property (weak, nonatomic) IBOutlet UILabel *backCardLabel;

@property (nonatomic, retain) cropperView *cropViewFront;
@property (nonatomic, retain) cropperView *cropViewBack;
@property (strong, nonatomic) IBOutlet UIButton *cropButton;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;
@property (strong, nonatomic) IBOutlet UIButton *exitCropButton;
@property (strong, nonatomic) UIView *croppingBackground;
@property (strong, nonatomic) IBOutlet UIButton *frontBackButton;
@property (strong, nonatomic) UILabel *frontCropLabel;
@property (strong, nonatomic) UILabel *backCropLabel;
@property (strong, nonatomic) helpPopUp *helpPop;
@property (weak, nonatomic) IBOutlet UIButton *closeAdButton;


//to be used for editing card only
@property (strong, nonatomic) CardData *cardForEdit;

@property FCPopUp *choosePopUp;
@property namePopUp *deckNameSelector;
@property enterTextPopUp *enterText;
@property categoryPopUp *categoryPop;
@property alertPopUp *alertPop;
@property NSString *mode;
@property (strong, nonatomic) IBOutlet UIImageView *cardSavedPopUp;

@property (strong, nonatomic) UITapGestureRecognizer *deckNameTapGesture;

@property bool front;

- (IBAction)exitCropPressed:(id)sender;
- (IBAction)nameButtonPressed:(id)sender;
- (IBAction)categoryButton:(id)sender;
- (IBAction)saveCard:(UIButton *)sender;
- (IBAction)resetCrop:(id)sender;
- (IBAction)cropImage:(id)sender;
- (IBAction)frontBackButtonPressed:(id)sender;
- (IBAction)helpPressed:(id)sender;
- (IBAction)closeAd:(id)sender;

@end
