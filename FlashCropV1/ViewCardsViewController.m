//
//  fcv1SecondViewController.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "ViewCardsViewController.h"

@interface ViewCardsViewController ()

@end

@implementation ViewCardsViewController{
    ADBannerView *_bannerView;
}
@synthesize backgroundView;
@synthesize cardBackground;
@synthesize nextCardArrow;
@synthesize previousCardArrow;
@synthesize frontView;
@synthesize backView;
@synthesize correctButton;
@synthesize incorrectButton;
@synthesize pinchScreen;
@synthesize dummyCardBackground;
@synthesize backLabel;
@synthesize frontLabel;
@synthesize backImage;
@synthesize frontImage;
@synthesize correctPopUp;
@synthesize incorrectPopUp;
@synthesize favoriteButton;
@synthesize deck;
@synthesize card;
@synthesize deleteButton;
@synthesize exitFullscreenButton;
@synthesize alertPop;
@synthesize closeAdButton;

NSMutableArray *cardArray;

int cardIndex; 
int totalRight = 0;
int totalWrong = 0;
bool frontViewIsVisible = true;
bool correct = false;
bool incorrect = false;
bool favorite = false;
bool fullscreen = false;
bool isRetina;
bool deletedDeck = false;
NSDate *startTime;

- (void)viewDidLoad
{
#ifdef LITE_VER
    _bannerView = [[ADBannerView alloc] init];
    _bannerView.delegate = self;
    _bannerView.frame=CGRectMake(0, 480, 320, 50);
    [self.view addSubview:_bannerView];
#endif
    self.closeAdButton.alpha=0;
    //google analytics page view
    NSError *error;
    
    if (![[GANTracker sharedTracker] trackPageview:@"/mycard_visit/viewcard_visit"
                                         withError:&error]) {
        // Handle error here
    }
    
    //Variables to control the parent class
    self.title = self.deck.deckName;
    self.navBar = true;
    self.back = true;
    self.correctPopUp.alpha = 0;
    self.incorrectPopUp.alpha = 0;
    self.exitFullscreenButton.hidden = TRUE;
    isRetina = ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))?1:0;
    //Set the background colors of the correct and incorrect buttons so it's 100% the correct color
    
    [super viewDidLoad];
    //Set background of the card
    self.cardBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardBackground.png"]];
    self.dummyCardBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardBackground.png"]];
    
    //Gesture Recognizers
    UISwipeGestureRecognizer *oneFingerSwipeLeft = [[UISwipeGestureRecognizer alloc] 
                                                    initWithTarget:self 
                                                    action:@selector(oneFingerSwipeLeft:)];
    [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:oneFingerSwipeLeft];
    
    UISwipeGestureRecognizer *oneFingerSwipeRight = [[UISwipeGestureRecognizer alloc] 
                                                     initWithTarget:self 
                                                     action:@selector(oneFingerSwipeRight:)];
    [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:oneFingerSwipeRight];
    
    
    UITapGestureRecognizer *tapScreen = [[UITapGestureRecognizer alloc] 
                                         initWithTarget:self 
                                         action:@selector(screenTapped:)];
    [self.cardBackground addGestureRecognizer:tapScreen];
    
    
    UISwipeGestureRecognizer *oneFingerSwipeUp = [[UISwipeGestureRecognizer alloc] 
                                                  initWithTarget:self 
                                                  action:@selector(correctButtonPressed:)];
    [oneFingerSwipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [[self view] addGestureRecognizer:oneFingerSwipeUp];
    
    
    UISwipeGestureRecognizer *oneFingerSwipeDown = [[UISwipeGestureRecognizer alloc] 
                                                    initWithTarget:self 
                                                    action:@selector(incorrectButtonPressed:)];
    [oneFingerSwipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [[self view] addGestureRecognizer:oneFingerSwipeDown];
    
    
    pinchScreen = [[UITapGestureRecognizer alloc] 
                   initWithTarget:self 
                   action:@selector(pinchDetected:)];
    [pinchScreen setNumberOfTouchesRequired:2];
    [[self view] addGestureRecognizer:pinchScreen];
    
    
    //Load card images
    self.frontView.backgroundColor = [UIColor blackColor];
    self.backView.backgroundColor = [UIColor whiteColor];
    
    self.frontView.frame = CGRectMake(0, 0, 0, 0);
    self.backView.frame = self.frontView.frame;
    
    
    [backView removeFromSuperview];
    
    //alert pop ove
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"alertPopUp" owner:self options:nil];
    self.alertPop = [subviewArray objectAtIndex:0];
    [self.alertPop style];
    [self.view addSubview:self.alertPop];
    self.alertPop.alpha = 0;

    
    //load first card
    DeckData *deckLocal = [coreServices isDeckPresent:self.deck.deckName];
    if (deckLocal != nil) {
        [cardArray removeAllObjects];
        cardIndex = 0;
        cardArray = [[deckLocal.toCard allObjects] mutableCopy];
        //default sorting is by creation date
        NSSortDescriptor *cardSort = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES];
        NSArray *sortDescriptor = [NSArray arrayWithObject:cardSort];
        [cardArray sortUsingDescriptors:sortDescriptor];
        
        for (CardData *cd in cardArray) {
            NSLog(@"Card creation date: %@", cd.createdAt);
        }
        
        //sort cardArray accoring to the settings
        NSMutableArray *sortedArray = [[NSMutableArray alloc] init];
        //prioritize favorite
        if([[[[coreServices getUserData] objectAtIndex:0] prioritizeFavorite] isEqualToNumber:[NSNumber numberWithInt:1]])
        {
            for (CardData *cd in cardArray) {
                if ([cd.favorite isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    [sortedArray addObject:cd];
                }
            }
            for (CardData *cd in cardArray) {
                if ([cd.favorite isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    [sortedArray addObject:cd];
                }
            }
            
            [cardArray removeAllObjects];
            cardArray = sortedArray;
        }
        //prioritize incorrect cards
        else if ([[[[coreServices getUserData] objectAtIndex:0] prioritizeWrongCards] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            for (CardData *cd in cardArray) {
                if ([cd.wrong isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    [sortedArray addObject:cd];
                }
            }
            for (CardData *cd in cardArray) {
                if ([cd.wrong isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    [sortedArray addObject:cd];
                }
            }
            
            [cardArray removeAllObjects];
            cardArray = sortedArray;
        }
        
        self.card = [cardArray objectAtIndex:0];
    }
    if(isRetina){
    }

    //DISPLAY THE CARD
    [self presentNextCard:self.card]; 
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.tabBarController.tabBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarMyCards.png"]]  atIndex:0]; 
    
    UIButton *editButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 54, 30)];
    [editButton setImage:[UIImage imageNamed:@"editButton.png"] forState:UIControlStateNormal];
    [editButton setShowsTouchWhenHighlighted:TRUE];
    [editButton addTarget:self action:@selector(editButtonPushed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:editButton];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.rightBarButtonItem = barBackItem;
    
    //start stopwatch
    startTime = [NSDate date];   
}

-(void)viewWillDisappear:(BOOL)animated{
    
    //get the aggragation of study result
//    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
//    int totalRight = 0;
//    int totalWrong = 0;
//    int totalResponse = 0;
//    for (CardData *cd in [[coreServices isDeckPresent:self.deck.deckName] toCard]) {
//        if ([cd.right isEqualToNumber:positive] || [cd.wrong isEqualToNumber:positive]) {
//            totalResponse++;
//            if ([cd.right isEqualToNumber:positive]) {
//                totalRight++;
//            }
//            if ([cd.wrong isEqualToNumber:positive]) {
//                totalWrong++;
//            }            
//        }
//    }
    
    NSLog(@" total right %i", totalRight);
    NSLog(@" total wrong %i", totalWrong);
    

    
    NSTimeInterval timeStudied = [[NSDate date] timeIntervalSinceDate:startTime];  
    
    if (!deletedDeck){
        [coreServices insertResult:self.deck :[NSNumber numberWithInt:totalRight] :[NSNumber numberWithInt:totalWrong] :timeStudied];
    }
    totalRight = 0;
    totalWrong = 0;
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setCardBackground:nil];
    [self setNextCardArrow:nil];
    [self setPreviousCardArrow:nil];
    [self setFrontView:nil];
    [self setBackView:nil];
    [self setCorrectButton:nil];
    [self setIncorrectButton:nil];
    [self setDummyCardBackground:nil];
    [self setDummyCardBackground:nil];
    [self setFrontLabel:nil];
    [self setBackLabel:nil];
    [self setFrontLabel:nil];
    [self setCorrectPopUp:nil];
    [self setIncorrectPopUp:nil];
    [self setFavoriteButton:nil];
    [self setExitFullscreenButton:nil];
    [self setCloseAdButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)oneFingerSwipeLeft:(UITapGestureRecognizer *)recognizer {
    self.nextCardArrow.alpha = 1;
    self.nextCardArrow.frame = CGRectMake(320, 116, 67, 135);
    [UIView animateWithDuration:0.2 animations:^{
        self.nextCardArrow.frame = CGRectMake(253, 116, 67, 135);
    } completion:^(BOOL finished){
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        self.nextCardArrow.alpha = 0;
        [UIView commitAnimations];
        
    }];
    
    [UIView animateWithDuration:0.2 animations:^{
        if(!fullscreen){
            self.cardBackground.frame = CGRectMake(-320, 15, 302, 352);
        }
        else{
            self.cardBackground.frame = CGRectMake(-320, 15, 302, 460);
        }
    } completion:^(BOOL finished){
        //START CHANGING CONTENT OF CARD 
        //go to next card
                
        //shuffle card
        if ([[[[coreServices getUserData] objectAtIndex:0] studyShuffle] isEqualToNumber:[NSNumber numberWithInt:1]] && cardArray.count>1) {
            cardIndex = (arc4random() % (cardArray.count-1)) + 1;
        }
        else {
            cardIndex++;
            //check if the last card is reached
            if(cardIndex >= cardArray.count){
                if ([[[[coreServices getUserData] objectAtIndex:0] studyRepeat] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    cardIndex = 0;
                }
                else{
                    //reached the last card
                    //fire alert pop
                    if(_bannerView.bannerLoaded){
                        self.closeAdButton.alpha=1;
                        _bannerView.hidden=false;
                    }
                    self.alertPop.alertMessageLabel.text = @"Last Card Reached!";
                    self.alertPop.alpha = 1;
                    self.alertPop.buttonOne.alpha = 0;
                    self.alertPop.buttonTwo.alpha = 0;
                    cardIndex--;
                }
            }
        }
        self.card = [cardArray objectAtIndex:cardIndex];
        
        //present next card
        [self presentNextCard:self.card];
        
        if(!fullscreen){
            self.cardBackground.frame = CGRectMake(320, 15, 302, 352);
        }
        else{
            self.cardBackground.frame = CGRectMake(320, 15, 302, 460);
        }
        
        
        //STOP CHANGING CONTENT OF THE CARD
        [UIView animateWithDuration:0.2 animations:^{
            if(!fullscreen){
                self.cardBackground.frame = CGRectMake(9, 15, 302, 352);
            }
            else{
                self.cardBackground.frame = CGRectMake(9, 15, 302, 460);
            }
        } completion:^(BOOL finished){
            
            if(!frontViewIsVisible){
                [self.backView removeFromSuperview];
                [self.cardBackground addSubview:self.frontView];
                frontViewIsVisible = true;
            }
        }];
        
    }];
}

- (void)oneFingerSwipeRight:(UITapGestureRecognizer *)recognizer {
    self.previousCardArrow.alpha = 1;
    
    self.previousCardArrow.frame = CGRectMake(-67, 116, 67, 135);
    [UIView animateWithDuration:0.2 animations:^{
        self.previousCardArrow.frame = CGRectMake(0, 116, 67, 135);
    } completion:^(BOOL finished){
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        self.previousCardArrow.alpha = 0;
        [UIView commitAnimations];
    }];
    
    [UIView animateWithDuration:0.2 animations:^{
        if(!fullscreen){
            self.cardBackground.frame = CGRectMake(320, 15, 302, 352);
        }
        else{
            self.cardBackground.frame = CGRectMake(320, 15, 302, 460);
        }
    } completion:^(BOOL finished){
        //START CHANGING CONTENT OF THE CARD previous card
        //shuffle card
        if ([[[[coreServices getUserData] objectAtIndex:0] studyShuffle] isEqualToNumber:[NSNumber numberWithInt:1]] && cardArray.count > 1) {
            cardIndex = (arc4random() % (cardArray.count-1)) + 1;
        }
        else {
            cardIndex--;
        //check if the first card is reached
            if(cardIndex < 0){
                if ([[[[coreServices getUserData] objectAtIndex:0] studyRepeat] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                    cardIndex = cardArray.count -1;
                }
                else{
                    //reached the first card
                    //fire alert pop
                    self.alertPop.alertMessageLabel.text = @"First Card Reached!";
                    self.alertPop.buttonOne.alpha = 0;
                    self.alertPop.buttonTwo.alpha = 0;
                    self.alertPop.alpha = 1;
                    
                    cardIndex++;
                }
            }
        }
        
        self.card = [cardArray objectAtIndex:cardIndex];
        
        //present next card
        [self presentNextCard:self.card];
//        
//        self.incorrectButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   
//        self.correctButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   
        if(!fullscreen){
            self.cardBackground.frame = CGRectMake(-320, 15, 302, 352);
        }
        else{
            self.cardBackground.frame = CGRectMake(-320, 15, 302, 460);
        }
        //STOP CHANGING CONTENT OF THE CARD
        [UIView animateWithDuration:0.2 animations:^{
            if(!fullscreen){
                self.cardBackground.frame = CGRectMake(9, 15, 302, 352);
            }
            else{
                self.cardBackground.frame = CGRectMake(9, 15, 302, 460);
            }
        } completion:^(BOOL finished){
            
            if(!frontViewIsVisible){
                [self.backView removeFromSuperview];
                [self.cardBackground addSubview:self.frontView];
                frontViewIsVisible = true;
            }
        }];
        
    }];
    
}

//method for generating random numeber
//method for loading content of any card to study view
-(void)presentNextCard:(CardData *)cardToPresent{
    if (cardToPresent != nil){
        
        //reset buttons
        correct = false;
        incorrect = false;
        
        self.incorrectButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   
        self.correctButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   

        //load last result  //blocked for current version Arko 09/11/2012
        if([cardToPresent.right isEqualToNumber:[NSNumber numberWithInt:1]]){
            self.correctButton.backgroundColor = [UIColor colorWithRed:0.184 green:0.533 blue:0.227 alpha:1];
            correct = true; 
        }
        else if([cardToPresent.wrong isEqualToNumber:[NSNumber numberWithInt:1]]){
            self.incorrectButton.backgroundColor = [UIColor colorWithRed:0.929 green:0.494 blue:0.219 alpha:1];
            incorrect = true;   
        }
        
        //load the favorite
        favorite = false;
        self.favoriteButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];; 
        if([cardToPresent.favorite isEqualToNumber:[NSNumber numberWithInt:1]]){
            self.favoriteButton.backgroundColor = [UIColor colorWithRed:0.184 green:0.533 blue:0.227 alpha:1]; 
            favorite = true;
        }
        
        //load front of the card
        if ([cardToPresent.frontContains isEqualToString:@"image"]) {
            UIImage *imageRef = self.card.frontImage;
            CGSize imageSize = imageRef.size;
            int maxWidth = 282;
            if(isRetina){
                maxWidth = 564;
            }
            if(imageSize.width > maxWidth){
                int ratio = imageSize.width/maxWidth;
                imageSize.width = maxWidth;
                imageSize.height = imageSize.height*ratio;
                
            }
            if(isRetina){
                self.frontImage.frame = CGRectMake((302-imageSize.width/2)/2, (352-imageRef.size.height/2)/2, imageSize.width/2, imageRef.size.height/2);
            }
            else{
                
                self.frontImage.frame = CGRectMake((302-imageSize.width)/2, (352-imageRef.size.height)/2, imageSize.width, imageRef.size.height);
            }
            self.frontImage.image = cardToPresent.frontImage;
            self.frontLabel.hidden = true;
            self.frontImage.hidden = false;
        }
        else if([cardToPresent.frontContains isEqualToString:@"text"]) {
            self.frontLabel.text = cardToPresent.frontText;
            self.frontImage.hidden = true;
            self.frontLabel.hidden = false;
        }
        //load back of the card
        if ([cardToPresent.backContains isEqualToString:@"image"]) {
            
            UIImage *imageRef = cardToPresent.backImage;
            CGSize imageSize = imageRef.size;
            int maxWidth = 282;
            if(isRetina){
                maxWidth = 564;
            }
            if(imageSize.width > maxWidth){
                int ratio = imageSize.width/maxWidth;
                imageSize.width = maxWidth;
                imageSize.height = imageSize.height*ratio;
                
            }
            
            if(isRetina){
                self.backImage.frame = CGRectMake((302-imageSize.width/2)/2, (352-imageRef.size.height/2)/2, imageSize.width/2, imageRef.size.height/2);
            }
            else{
                
                self.backImage.frame = CGRectMake((302-imageSize.width)/2, (352-imageRef.size.height)/2, imageSize.width, imageRef.size.height);
            }
            self.backImage.image = cardToPresent.backImage;
            self.backLabel.hidden = true;
            self.backImage.hidden = false;
        }
        else if([cardToPresent.backContains isEqualToString:@"text"]) {
            self.backLabel.text = cardToPresent.backText;
            self.backImage.hidden = true;
            self.backLabel.hidden = false;
        }
    }
}

-(void) moveCardFromCorner{
    self.cardBackground.frame = CGRectMake(320, 480, 302, 352);
    [UIView animateWithDuration:0.5 animations:^{
        self.cardBackground.frame = CGRectMake(9, 15, 302, 352);
    } completion:^(BOOL finished){
        self.dummyCardBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardBackground.png"]];
        self.dummyCardBackground.frame = CGRectMake(9, 15, 302, 352);
        [self.view bringSubviewToFront:self.cardBackground];
        self.cardBackground.frame = CGRectMake(320, 480, 302, 352);
    }];
    
}

- (void) editButtonPushed{
    
    //    [self presentViewController:createView animated:YES completion:nil];
    [self performSegueWithIdentifier:@"toCreate" sender:self];
    
}


- (IBAction) pinchDetected:(id)sender{
    
    if(!fullscreen){
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        for(UIView *view in self.tabBarController.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
            } 
            else 
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
            }
            
        }
        
        [UIView commitAnimations];
        self.backgroundView.frame = CGRectMake(0, 0, 320, 460);
        self.frontLabel.frame = CGRectMake(0, 0, 302, 460);
        self.backLabel.frame = CGRectMake(0, 0, 302, 460);
        
        self.correctButton.hidden = true;
        self.incorrectButton.hidden = true;
        self.favoriteButton.hidden = true;
        self.deleteButton.hidden = true;
        
        self.cardBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardBackgroundLarge.png"]];
        self.exitFullscreenButton.hidden = FALSE;
    }
    else{
        [[self navigationController] setNavigationBarHidden:NO animated:YES];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.5];
        for(UIView *view in self.tabBarController.view.subviews)
        {
            NSLog(@"%@", view);
            
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
                
            } 
            else 
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
            }
            
            
        }
        
        [UIView commitAnimations]; 
        self.backgroundView.frame = CGRectMake(0, 0, 320, 420);
        self.cardBackground.frame = CGRectMake(9, 15, 302, 352);
        
        self.frontLabel.frame = CGRectMake(0, 0, 302, 352);
        self.backLabel.frame = CGRectMake(0, 0, 302, 352);
        self.correctButton.hidden = false;
        self.incorrectButton.hidden = false;
        self.favoriteButton.hidden = false;
        self.deleteButton.hidden = false;
        self.cardBackground.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardBackground.png"]];
        self.exitFullscreenButton.hidden = TRUE;
        
    }
    fullscreen = !fullscreen;
    
    
}

- (IBAction)closeAd:(id)sender {
    self.closeAdButton.alpha=0;
    _bannerView.hidden=true;
}


- (IBAction)screenTapped:(id)sender {
    self.cardBackground.userInteractionEnabled = YES;
    
    // setup the animation group
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(myTransitionDidStop:)];
    
    // swap the views and transition
    if (frontViewIsVisible==YES) {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:cardBackground cache:YES];
        [frontView removeFromSuperview];
        [cardBackground addSubview:backView];
        
        
    } else {
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:cardBackground cache:YES];
        [backView removeFromSuperview];
        [cardBackground addSubview:frontView];
    }
    [UIView commitAnimations];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(myTransitionDidStop:)];
    
    [UIView commitAnimations];
    frontViewIsVisible=!frontViewIsVisible;
    
    
}

- (IBAction)correctButtonPressed:(id)sender {
    
    if(correct){
        //fires when already correct 
        self.correctButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   
        correct = false;
        //card correct value is back to 0
        self.card.right = [[NSNumber alloc] initWithInt:0];
        totalRight--;
        
    }
    else if (incorrect){
        //fires when the card is already incorrect
        [UIView animateWithDuration:0.2 animations:^{
            self.correctPopUp.alpha = 1;
        } completion:^(BOOL finished){    
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.2];
            self.correctPopUp.alpha = 0;
            [UIView commitAnimations];
            
        }];
        
        correct = true;
        incorrect = false;
        self.incorrectButton.backgroundColor =  [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];
        self.correctButton.backgroundColor = [UIColor colorWithRed:0.184 green:0.533 blue:0.227 alpha:1];
        
        //make card wrong to 0 and right to 1
        self.card.right = [[NSNumber alloc] initWithInt:1];
        totalRight++;
        self.card.wrong = [[NSNumber alloc] initWithInt:0];
        totalWrong--;
    }
    else{
        //fires when card is neither correct or incorrect
        [UIView animateWithDuration:0.2 animations:^{
            self.correctPopUp.alpha = 1;
        } completion:^(BOOL finished){    
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.2];
            self.correctPopUp.alpha = 0;
            [UIView commitAnimations];
            
        }];
        self.correctButton.backgroundColor = [UIColor colorWithRed:0.184 green:0.533 blue:0.227 alpha:1];
        correct = true;
        self.card.right = [[NSNumber alloc] initWithInt:1];
        totalRight++;
    }
    
    // register stat
    [coreServices saveCard:card];
    
}

- (IBAction)incorrectButtonPressed:(id)sender {
    
    if(incorrect){
        //fires when the card is already incorrect
        self.incorrectButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];   
        incorrect = false;
        self.card.wrong = [[NSNumber alloc] initWithInt:0];
        totalWrong--;
    }
    else if (correct){
        //fires when the card is already correct
        [UIView animateWithDuration:0.2 animations:^{
            self.incorrectPopUp.alpha = 1;
        } completion:^(BOOL finished){    
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.2];
            self.incorrectPopUp.alpha = 0;
            [UIView commitAnimations];
            
        }];
        
        correct = false;
        incorrect = true;
        self.incorrectButton.backgroundColor =  [UIColor colorWithRed:0.929 green:0.494 blue:0.219 alpha:1];
        self.correctButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];
        self.card.right = [[NSNumber alloc] initWithInt:0];
        totalRight--;
        self.card.wrong = [[NSNumber alloc] initWithInt:1];
        totalWrong++;
    }
    else{
        // fires when card is neither corrrect or incorrect
        [UIView animateWithDuration:0.2 animations:^{
            self.incorrectPopUp.alpha = 1;
        } completion:^(BOOL finished){    
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.2];
            self.incorrectPopUp.alpha = 0;
            [UIView commitAnimations];
            
        }];
        
        self.incorrectButton.backgroundColor = [UIColor colorWithRed:0.929 green:0.494 blue:0.219 alpha:1];
        incorrect = true;
        self.card.wrong = [[NSNumber alloc] initWithInt:1];
        totalWrong++;
    }
    
    // register stat
    [coreServices saveCard:card];
}

- (IBAction)favoriteButtonPushed:(id)sender {
    
    //can we play witht the color a bit??
    if([self.card.favorite isEqualToNumber:[[NSNumber alloc] initWithInt:1]]){
        self.card.favorite = [[NSNumber alloc] initWithInt:0];
        self.favoriteButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];
    }
    else {
        self.card.favorite = [[NSNumber alloc] initWithInt:1];
        self.favoriteButton.backgroundColor = [UIColor colorWithRed:0.184 green:0.533 blue:0.227 alpha:1];         
    }
    // register stat
    [coreServices saveCard:card];
}

- (IBAction)deleteCard:(UIButton *)sender {
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"alertPopUp" owner:self options:nil];
    alertPop = [subviewArray objectAtIndex:0];
    alertPop.delegate = self;
    [alertPop style];
    [self.view addSubview:alertPop];
    alertPop.alertMessageLabel.text = @"Card will be deleted permanently!";
    alertPop.alpha = 1;
    
    
    
}
//A9
//alertPopup delegate
- (void) alertPopUpFinished :(bool)confirmed{
    if (confirmed){
        // delete card
        [coreServices deleteCard:self.card];
        // check if the card is the last card 
        if (cardArray.count < 2 ) {
            //delete the deck also
//            DeckData *deckLocal = self.card.toDeck;
//            NSLog(@"%@",deckLocal.deckName);
//            [coreServices deleteDeck:deckLocal.deckName];
            //Go back to my cards screen
            deletedDeck = true;
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            //present the next card available
            [cardArray removeObject:self.card];
            deletedDeck  = false;
            //shuffle card
            if ([[[[coreServices getUserData] objectAtIndex:0] studyShuffle] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                cardIndex = (arc4random() % (cardArray.count-1)) + 1;
            }
            else {
                cardIndex++;
                //check if the last card is reached
                if(cardIndex >= cardArray.count){
                    if ([[[[coreServices getUserData] objectAtIndex:0] studyRepeat] isEqualToNumber:[NSNumber numberWithInt:1]]) {
                        cardIndex = 0;
                    }
                    else{
                        //reached the last card
                        //fire alert pop
                        
                        
                        cardIndex--;
                    }
                }
            }
            self.card = [cardArray objectAtIndex:cardIndex];
            
            //present next card
            [self presentNextCard:self.card];
        }
    }
}

-(void)myTransitionDidStop{
    self.cardBackground.userInteractionEnabled = YES;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"toCreate"]) {  
        
        NSLog(@"Segue activated");
        CreateCardsViewController *createView = segue.destinationViewController;
        createView.cardForEdit = card;
        createView.mode = @"Edit";
        
    }
    
}


- (void)layoutAnimated:(BOOL)animated
{
            _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;

    
    CGRect bannerFrame = _bannerView.frame;
        bannerFrame = CGRectMake(0, 315, 320, 50);
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _bannerView.frame = bannerFrame;
    }];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
        return YES;
}






@end
