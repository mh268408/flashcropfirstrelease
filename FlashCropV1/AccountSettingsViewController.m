//
//  AccountSettingsViewController.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "AccountSettingsViewController.h"

@interface AccountSettingsViewController ()

@end

@implementation AccountSettingsViewController
@synthesize backgroundView;
@synthesize containerView;
@synthesize nameBox;
@synthesize email;
@synthesize userPicture;

UIImagePickerController * picker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.back = true;
    self.navBar = true;
    self.nameBox.delegate = self;
    self.email.delegate = self;
    self.title = @"ACCOUNT";
    NSLog(@"%@", [[[coreServices getUserData] objectAtIndex:0] userName]);

    self.nameBox.text = [[[coreServices getUserData] objectAtIndex:0] userName];
    self.email.text = [[[coreServices getUserData] objectAtIndex:0] email];
    self.userPicture.image = [[[coreServices getUserData] objectAtIndex:0] userPicture];
    
    
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarSettings.png"]]  atIndex:0]; 
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setNameBox:nil];
    [self setUserPicture:nil];
    [self setEmail:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch * touch = [touches anyObject];
    if(touch.phase == UITouchPhaseBegan) {
        [self.nameBox resignFirstResponder];
        [self.email resignFirstResponder];
    }
}
- (IBAction)changePicturePressed:(id)sender {
    
    picker = nil;
    if (picker == nil) {   
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
        
    } 
    [self.tabBarController presentModalViewController:picker animated:YES];
 
}

- (IBAction)createAccountPressed:(id)sender {
    
    //save Settings in core service
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        
                                 self.nameBox.text, @"userName",
                                 self.email.text, @"email",
                                 self.userPicture.image, @"userPicture", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                                                        positive, @"userName",
                                positive, @"email",
                                positive, @"userPicture", 
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)selectedImage editingInfo:(NSDictionary *)editingInfo {
    
    self.userPicture.image = selectedImage;
    [picker dismissModalViewControllerAnimated:YES];
}
@end
