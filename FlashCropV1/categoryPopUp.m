//
//  categoryPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "categoryPopUp.h"

@implementation categoryPopUp
@synthesize closeButton;
@synthesize label;
@synthesize button1;
@synthesize button2;
@synthesize delegate;
@synthesize content;
@synthesize background;
@synthesize table;
@synthesize namePop;
@synthesize alertPop;
@synthesize mode;
@synthesize addButtonMode;

NSMutableArray *categoryList;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void) style{
    
    self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    self.background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.75];
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"namePopUp" owner:self options:nil];
    self.namePop = [subviewArray objectAtIndex:0];
    
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    self.namePop.button1Title = @"DONE";
    self.namePop.button2Title = @"CANCEL";
    [self.namePop style];
    self.namePop.cancel = true;
    self.namePop.title.text = @"CATEGORY";
    self.namePop.delegate = self;
    self.namePop.closeButton.alpha = 0;
    self.namePop.background.alpha = 0;
    [self addSubview:self.namePop];
    [self bringSubviewToFront:closeButton];
    self.namePop.alpha = 0;
    self.button1.userInteractionEnabled = false;
    self.table.delegate =  self;
    self.table.dataSource = self;
    
    //alert pop ove
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"alertPopUp" owner:self options:nil];
    self.alertPop = [subviewArray objectAtIndex:0];
    [self.alertPop style];
    [self addSubview:self.alertPop];
    self.alertPop.alpha = 0;
    
    if ([self.mode isEqualToString:@"Category"]){
        UserData *user = [[coreServices getUserData] objectAtIndex:0];
        categoryList = [[user.toCategory allObjects] mutableCopy];
    }
    else if([self.mode isEqualToString:@"Deck"]){
        categoryList = [coreServices getDeckData];
    }
}

- (IBAction)selectButtonPushed:(id)sender {
    
    if ([self.mode isEqualToString:@"Category"]){
        [[self delegate] categoryFinished:[[categoryList objectAtIndex:self.table.indexPathForSelectedRow.row] category]];
    }
    else if([self.mode isEqualToString:@"Deck"]){
        [[self delegate] categoryFinished:[[categoryList objectAtIndex:self.table.indexPathForSelectedRow.row] deckName]];
    }
    
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    
}

- (IBAction)addButtonPushed:(id)sender {
    
    if ([self.mode isEqualToString:@"Category"]){
        if ([self.addButtonMode isEqualToString:@"Add"])
        {
            //executes for the createcard screen
            self.namePop.title.text = @"ADD CATEGORY";
            self.namePop.button1.titleLabel.text = @"DONE";
            self.namePop.button2.titleLabel.text = @"CANCEL";
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            self.content.alpha = 0;
            self.namePop.alpha = 1;
            [UIView commitAnimations];
        }
    }
    else if ([self.mode isEqualToString:@"Deck"]) {
        if ([self.addButtonMode isEqualToString:@"Add"])
        {
            //executes for the create card screen
            self.namePop.title.text = @"ADD DECK";
            self.namePop.button1.titleLabel.text = @"DONE";
            self.namePop.button2.titleLabel.text = @"CANCEL";
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            self.content.alpha = 0;
            self.namePop.alpha = 1;
            [UIView commitAnimations];
        }
        else {
            //executes for the statisstics screen
            [[self delegate] categoryFinished:@"ALL DECKS"];
            [UIView beginAnimations:@"fade out" context:nil];
            [UIView setAnimationDuration:0.5];
            self.alpha = 0;
            [UIView commitAnimations];
        }
    }
    
}


//delegate method for the namepopup
- (void) NamePopUpFinished: (NSString *)textExtract :(BOOL)buttonIndex{
    
    if (buttonIndex){
        
        if ([self.mode isEqualToString:@"Category"]){
            //add new Category
            bool isDuplicate = [coreServices addCategory:textExtract];
            
            if(isDuplicate){
                [UIView beginAnimations:@"fade out" context:nil];
                [UIView setAnimationDuration:0.5];
                self.alertPop.alertMessageLabel.text = @"Category already exists!";
                self.alertPop.alpha = 1;
                [UIView commitAnimations];
            }
            else {
                //close popup and add the Category as return text
                [[self delegate] categoryFinished:textExtract];
                [UIView beginAnimations:@"fade out" context:nil];
                [UIView setAnimationDuration:0.5];
                self.alpha = 0;
                [UIView commitAnimations];
            }
        }
        else if ([self.mode isEqualToString:@"Deck"]) {
            DeckData *deck = [coreServices isDeckPresent:textExtract];
            if (deck != nil) {
                //deck is already there
                [UIView beginAnimations:@"fade out" context:nil];
                [UIView setAnimationDuration:0.5];
                self.alertPop.alertMessageLabel.text = @"Deck already exists!";
                self.alertPop.alpha = 1;
                [UIView commitAnimations];
            }
            else {
                //close popup and add the deck as return text
                [[self delegate] categoryFinished:textExtract];
                [UIView beginAnimations:@"fade out" context:nil];
                [UIView setAnimationDuration:0.5];
                self.alpha = 0;
                [UIView commitAnimations];

            }
        }

    }
    
    else {

        //cancel button in namePop pressed
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDuration:0.5];
        self.content.alpha = 1;
        self.namePop.alpha = 0;
        [UIView commitAnimations];
    }
    
}

- (IBAction)closePopUp:(id)sender{
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
}


//configuring the tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%i", categoryList.count);
    return categoryList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if ([self.mode isEqualToString:@"Category"]){
        cell.textLabel.text = [[categoryList objectAtIndex:indexPath.row] category];
    }
    else if([self.mode isEqualToString:@"Deck"]){
        cell.textLabel.text = [[categoryList objectAtIndex:indexPath.row] deckName];
    }
    
        
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.button1.userInteractionEnabled = true;
    
}

@end
