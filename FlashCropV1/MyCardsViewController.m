//
//  MyCardsViewController.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "MyCardsViewController.h"

@interface MyCardsViewController ()

@end

@implementation MyCardsViewController
@synthesize editDeckPopUp;
@synthesize helpPop;

NSMutableArray *categoryList;
NSMutableArray *decksPerCategory;
NSString *sectionText;
NSIndexPath *indexPath; //for capturing the index of the editAccessory View


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    categoryList = [[NSMutableArray alloc] init];
    decksPerCategory = [[NSMutableArray alloc] init];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"deckPopUp" owner:self options:nil];
    self.editDeckPopUp = [subviewArray objectAtIndex:0];
    [self.editDeckPopUp style];
    self.editDeckPopUp.delegate = self;
    [self.view addSubview:self.editDeckPopUp];
    self.editDeckPopUp.alpha = 0;
    
    subviewArray = [[NSBundle mainBundle] loadNibNamed:@"helpView" owner:self options:nil];
    self.helpPop = [subviewArray objectAtIndex:0];
    self.helpPop.fileName = @"MYCARDHELP";
    [self.helpPop style];
    [self.view addSubview:self.helpPop];
    self.helpPop.alpha = 0;
    
    //google analytics page view
    NSError *error;
    if (![[GANTracker sharedTracker] trackPageview:@"/mycard_visit"
                                         withError:&error]) {
        // Handle error here
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    bool isPresent;
    [categoryList removeAllObjects];
    //get categories in categoryList
    [coreServices loadData]; //reload
    for (DeckData *deck in [coreServices getDeckData]) {
                NSLog(@"deck deckName %@", deck.deckName);
                NSLog(@"deck category %@", deck.category);
        isPresent = false;
        for (NSString *cat in categoryList) {
            if ([cat isEqualToString:deck.category]) {
                isPresent = true;
                break;
            }
        }
        if (!isPresent) { 
            [categoryList addObject:deck.category];
        }
    }
    [self.tableView reloadData];
}


//Delegates for tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return categoryList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //get the section text for the deck 
    sectionText = [categoryList objectAtIndex:section];
    NSLog(@"section text %@", sectionText);
    [decksPerCategory removeAllObjects];
    //get all the decks for a particular section
    for (DeckData *deck in [coreServices getDeckData]) {
        NSLog(@"Deck Category %@", deck.category);
        NSLog(@"section text %@", sectionText);
        if([deck.category isEqualToString:sectionText]){
            [decksPerCategory addObject:deck];
        }
    }       
    
    return decksPerCategory.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,271,51)];
    label.backgroundColor = [UIColor clearColor];
    
    label.text = [categoryList objectAtIndex:section];
    
    label.text = [label.text uppercaseString]; 
    [label setTextAlignment:UITextAlignmentCenter];
    label.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sectionHeaderBackground.png"]];
    label.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:33.33];
    label.textColor = [UIColor blackColor];
    return label;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //get the section text for the deck 
    sectionText = [categoryList objectAtIndex:indexPath.section];
    NSLog(@"section text %@", sectionText);
    [decksPerCategory removeAllObjects];
    //get all the decks for a particular section
    for (DeckData *deck in [coreServices getDeckData]) {
        NSLog(@"Deck Category %@", deck.category);
        NSLog(@"section text %@", sectionText);
        if([deck.category isEqualToString:sectionText]){
            [decksPerCategory addObject:deck];
        }
    }      
    
    cell.textLabel.text = [[decksPerCategory objectAtIndex:indexPath.row] deckName];
    cell.textLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20.83];
    
    
    DeckData *deckLocal = [decksPerCategory objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [[NSString stringWithFormat:@"%@", @"Total Cards: "] stringByAppendingFormat:@"%i", deckLocal.toCard.count];
    
    //editing accessory view
    UIButton *editAccessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    editAccessoryButton.backgroundColor = [UIColor colorWithRed:0.388 green:0.392 blue:0.4 alpha:1];
    editAccessoryButton.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
    editAccessoryButton.titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    editAccessoryButton.titleLabel.textAlignment = UITextAlignmentCenter;
    editAccessoryButton.titleLabel.textColor =[UIColor colorWithWhite:1 alpha:1];
    [editAccessoryButton setFrame:CGRectMake(0, 0, 75, 35)];
    
    [editAccessoryButton setTitle:@"Edit" forState:UIControlStateNormal];
    [editAccessoryButton addTarget:self action:@selector(editAccessoryPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.editingAccessoryView = editAccessoryButton;
    
    return cell;
}

-(void)editAccessoryPressed:(id)sender{
    
    //get the index 
    UIButton *button = (UIButton *)sender;
    UITableViewCell *cell = (UITableViewCell *)button.superview;
    UITableView *tableView = (UITableView *)cell.superview;
    indexPath = [tableView indexPathForCell:cell];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.editDeckPopUp.alpha = 1;
    } completion:^(BOOL finished){    
        [self.editDeckPopUp.tableView flashScrollIndicators];
        
    }];
}

- (void) deckPopFinished: (NSString *)deckName :(NSString *)category{
    
    //need to save the new Deck Data
    for (DeckData *deck in [coreServices getDeckData]) {
        if ([deck.deckName isEqualToString:[[[self.tableView cellForRowAtIndexPath:indexPath] textLabel] text]] ) {
            deck.deckName = deckName;
            deck.category = category;
            [coreServices saveDeck:deck];
            break;
        }
    }
    
    //data sources change
    bool isPresent;
    [categoryList removeAllObjects];
    //get categories in categoryList
    for (DeckData *deck in [coreServices getDeckData]) {
        NSLog(@"deck deckName %@", deck.deckName);
        NSLog(@"deck category %@", deck.category);
        isPresent = false;
        for (NSString *cat in categoryList) {
            if ([cat isEqualToString:deck.category]) {
                isPresent = true;
                break;
            }
        }
        if (!isPresent) { 
            [categoryList addObject:deck.category];
        }
    }
    
    //track event for editing deck name
    NSError *error;
    if (![[GANTracker sharedTracker] trackEvent:@"mycard"
                                         action:@"editdeck"
                                          label:@""
                                          value:-1
                                      withError:&error]) {
        NSLog(@"Error: %@", error);
    }
    [self.tableView reloadData];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // If row is deleted, remove it from the list.
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // delete data from the data source
        [coreServices deleteDeck:[[[self.tableView cellForRowAtIndexPath:indexPath] textLabel] text]];
        
        // Animate the deletion from the table.
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];  
        
        bool isPresent;
        [categoryList removeAllObjects];
        //get categories in categoryList
        for (DeckData *deck in [coreServices getDeckData]) {
            NSLog(@"deck deckName %@", deck.deckName);
            NSLog(@"deck category %@", deck.category);
            isPresent = false;
            for (NSString *cat in categoryList) {
                if ([cat isEqualToString:deck.category]) {
                    isPresent = true;
                    break;
                }
            }
            if (!isPresent) { 
                [categoryList addObject:deck.category];
            }
        }
        [self.tableView reloadData];
        //track event for deleting deck
        NSError *error;
        if (![[GANTracker sharedTracker] trackEvent:@"mycard"
                                             action:@"deletedeck"
                                              label:@""
                                              value:-1
                                          withError:&error]) {
            NSLog(@"Error: %@", error);
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"toStudy"]) {  
        
        NSLog(@"Segue activated");
        ViewCardsViewController *destViewController = segue.destinationViewController;
        for (DeckData *deck in [coreServices getDeckData]) {
            if ([deck.deckName isEqualToString:[[[self.tableView cellForRowAtIndexPath:self.tableView.indexPathForSelectedRow] textLabel] text]] ) {
                destViewController.deck = deck;
                break;
            }
        }
        
    }
    
}


- (IBAction)helpPressed:(id)sender {
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.helpPop.alpha = 1;
    [UIView commitAnimations];
}
@end
