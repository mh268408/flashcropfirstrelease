//
//  FCPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCPopUp.h"
#import <QuartzCore/QuartzCore.h>


@implementation FCPopUp
@synthesize background;
@synthesize content;
@synthesize buttonOne;
@synthesize buttonTwo;
@synthesize buttonThree;
@synthesize title;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if(self){
        self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    }
    return self;
}



- (void) style{
    self.background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.75];
    self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    self.content.layer.borderColor = [UIColor colorWithWhite:0.75 alpha:1].CGColor;
    self.content.layer.borderWidth = 1;
}

- (IBAction)button1Action:(UIButton *)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] FCPopUpFinished:1];
    
}

- (IBAction)button2Action:(UIButton *)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] FCPopUpFinished:2];
}

- (IBAction)button3Action:(UIButton *)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] FCPopUpFinished:3];
}

- (IBAction)closeButton:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] FCPopUpFinished:0];
}
@end
