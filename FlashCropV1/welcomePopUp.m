//
//  welcomePopUp.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 8/11/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "welcomePopUp.h"

@implementation welcomePopUp
@synthesize content;
@synthesize background;
@synthesize welcomeBox;
@synthesize nameBox;
@synthesize emailBox;
@synthesize userImage;
@synthesize delegate;

UIImagePickerController * picker;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void) style{
    self.welcomeBox.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    self.content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground"]];
    NSString *username = [[UIDevice currentDevice] name];
        
    username = [username stringByReplacingOccurrencesOfString:@"iphone" withString:@""];
    username = [username stringByReplacingOccurrencesOfString:@"Iphone" withString:@""];
    username = [username stringByReplacingOccurrencesOfString:@"iPhone" withString:@""];
    username = [username stringByReplacingOccurrencesOfString:@"IPHONE" withString:@""];
    username = [username stringByReplacingOccurrencesOfString:@"\'s" withString:@""];
     NSString *uppercase = [username uppercaseString];
    self.nameBox.text = uppercase;
    self.nameBox.delegate = self;
    self.emailBox.delegate = self;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)choosePicturePressed:(id)sender {
    
    picker = nil;
    if (picker == nil) {   
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = (id)self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.allowsEditing = YES;
        
    } 
//    [self.superview.wind presentModalViewController:picker animated:YES];
}

- (IBAction)donePressed:(id)sender {
    
    //create user with the data  
    [coreServices createUser:self.nameBox.text :self.emailBox.text :self.userImage.image];
    
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:1];
    //also load some default settings 
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 positive, @"autoDeckname", 
                                 @"Card", @"cardNamePrefix",  
                                 negative, @"prioritizeFavorite",  
                                 negative, @"prioritizeWrongCards", 
                                 negative, @"statInNumber", 
                                 negative, @"studyRepeat", 
                                 negative, @"studyShuffle", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:
                                positive, @"autoDeckname", 
                                positive, @"cardNamePrefix",  
                                positive, @"prioritizeFavorite",  
                                positive, @"prioritizeWrongCards", 
                                positive, @"statInNumber", 
                                positive, @"studyRepeat", 
                                positive, @"studyShuffle", 
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    
    
    [self.nameBox resignFirstResponder];
    [self.emailBox resignFirstResponder];
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] welcomeFinished];
    
}

- (IBAction)skipPressed:(id)sender {
    
    //create user with default
    NSString *userName;
    if ([self.nameBox.text isEqualToString:@""]) {
        userName = @"GuestUser";
    }
    [coreServices createUser:userName :self.emailBox.text :self.userImage.image];
    
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:1];
    //also load some default settings 
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 positive, @"autoDeckname", 
                                 @"Card", @"cardNamePrefix",  
                                 negative, @"prioritizeFavorite",  
                                 negative, @"prioritizeWrongCards", 
                                 negative, @"statInNumber", 
                                 negative, @"studyRepeat", 
                                 negative, @"studyShuffle", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:
                                positive, @"autoDeckname", 
                                positive, @"cardNamePrefix",  
                                positive, @"prioritizeFavorite",  
                                positive, @"prioritizeWrongCards", 
                                positive, @"statInNumber", 
                                positive, @"studyRepeat", 
                                positive, @"studyShuffle", 
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    
    [self.nameBox resignFirstResponder];
    [self.emailBox resignFirstResponder];
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    [[self delegate] welcomeFinished];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end


