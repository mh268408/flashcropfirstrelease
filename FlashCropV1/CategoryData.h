//
//  CategoryData.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 8/5/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserData;

@interface CategoryData : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) UserData *toUser;

@end
