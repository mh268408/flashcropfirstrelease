//
//  StatisticsViewController.h
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCUIView.h"
#import "PieChartView.h"
#import "categoryPopUp.h"
#import "coreServices.h"
#import "helpPopUp.h"
#import <iAd/iAd.h>


@interface StatisticsViewController : FCUIView <CategoryDelegate, UITableViewDelegate, UITableViewDataSource, ADBannerViewDelegate>
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet PieChartView *pieChart;
@property (weak, nonatomic) IBOutlet UIButton *deckNameButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UILabel *rightPiLabel;
@property (weak, nonatomic) IBOutlet UILabel *wrongPiLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCard;
@property (weak, nonatomic) IBOutlet UILabel *pictureToTextRatio;
@property (weak, nonatomic) IBOutlet UILabel *hoursStudied;
@property (weak, nonatomic) IBOutlet UILabel *lastStudied;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) categoryPopUp *categoryPop;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *textRatio;
@property (weak, nonatomic) IBOutlet UIButton *closeAdButton;
@property (strong, nonatomic) helpPopUp *helpPop;
- (IBAction)deckNamePressed:(UIButton *)sender;
- (IBAction)resetPressed:(UIButton *)sender;
- (IBAction)helpPressed:(id)sender;
- (IBAction)closeAd:(id)sender;

@end
