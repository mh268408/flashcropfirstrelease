//
//  SettingsViewController.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController{
    ADBannerView *_bannerView;
}

@synthesize backgroundView;
@synthesize containerView;
@synthesize helpPop;
@synthesize closeAdButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
#ifdef LITE_VER
    if(_bannerView.bannerLoaded){
        _bannerView.hidden=false;
        self.closeAdButton.alpha=1;
    }
#endif
    [super viewWillAppear:animated];
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarSettings.png"]]  atIndex:0]; 
}

- (void)viewDidLoad
{
#ifdef LITE_VER
    _bannerView = [[ADBannerView alloc] init];
    _bannerView.delegate = self;
    _bannerView.hidden = false;
    _bannerView.frame=CGRectMake(0, 480, 320, 50);
    [self.view addSubview:_bannerView];
#endif
    self.closeAdButton.alpha = 0;
    self.navBar = true;
    self.title = @"SETTINGS";
    [super viewDidLoad];  
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"helpView" owner:self options:nil];
    self.helpPop = [subviewArray objectAtIndex:0];
    self.helpPop.fileName = @"HELP";
    [self.helpPop style];
    self.helpPop.alpha = 0;
    [self.view addSubview:self.helpPop];
    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackgroundNav.png"]];
	// Do any additional setup after loading the view.
    //google analytics page view
    NSError *error;
    if (![[GANTracker sharedTracker] trackPageview:@"/settings_visit"
                                         withError:&error]) {
        // Handle error here
    }
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setCloseAdButton:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)helpPushed:(id)sender {
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.helpPop.alpha = 1;
    [UIView commitAnimations];
}

- (IBAction)closeAd:(id)sender {
    self.closeAdButton.alpha=0;
    _bannerView.hidden=true;
}



- (void)layoutAnimated:(BOOL)animated
{
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    
    
    CGRect bannerFrame = _bannerView.frame;
    bannerFrame = CGRectMake(0, 315, 320, 50);
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _bannerView.frame = bannerFrame;
    }];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}
@end
