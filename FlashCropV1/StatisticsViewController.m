//
//  StatisticsViewController.m
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 7/30/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "StatisticsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface StatisticsViewController ()

@end

@implementation StatisticsViewController{
    ADBannerView *_bannerView;
}

@synthesize backgroundView;
@synthesize containerView;
@synthesize pieChart = _pieChart;
@synthesize deckNameButton;
@synthesize resetButton;
@synthesize rightPiLabel;
@synthesize wrongPiLabel;
@synthesize totalCard;
@synthesize pictureToTextRatio;
@synthesize hoursStudied;
@synthesize lastStudied;
@synthesize table;
@synthesize helpPop;

@synthesize categoryPop;
@synthesize titleLabel;
@synthesize textRatio;
@synthesize closeAdButton;

NSMutableArray *history;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    #ifdef LITE_VER
    if(_bannerView.bannerLoaded){
        _bannerView.hidden=false;
        self.closeAdButton.alpha=1;
    }
    #endif   
    [self calculatePi:@"ALL DECKS"];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    #ifdef LITE_VER
    _bannerView = [[ADBannerView alloc] init];
    _bannerView.delegate = self;
    _bannerView.hidden = false;
    _bannerView.frame=CGRectMake(0, 480, 320, 50);
    [self.view addSubview:_bannerView];
    #endif
    
    self.closeAdButton.alpha = 0;
    self.titleLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"NavBackground.png"]];
	// Do any additional setup after loading the view.
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.table flashScrollIndicators];
    history = [[NSMutableArray alloc] init];
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"helpView" owner:self options:nil];
    self.helpPop = [subviewArray objectAtIndex:0];
    self.helpPop.fileName = @"STATHELP";
    [self.helpPop style];
    self.helpPop.alpha = 0;
    [self.view addSubview:self.helpPop];
    
    //google analytics page view
    NSError *error;
    if (![[GANTracker sharedTracker] trackPageview:@"/statistics_visit"
                                         withError:&error]) {
        // Handle error here
    }
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setPieChart:nil];
    [self setDeckNameButton:nil];
    [self setResetButton:nil];
    [self setRightPiLabel:nil];
    [self setWrongPiLabel:nil];
    [self setTotalCard:nil];
    [self setPictureToTextRatio:nil];
    [self setHoursStudied:nil];
    [self setLastStudied:nil];
    [self setTitleLabel:nil];
    [self setTable:nil];
    [self setTextRatio:nil];
    [self setCloseAdButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)deckNamePressed:(UIButton *)sender {
  
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"categoryPopUp" owner:self options:nil];
    self.categoryPop = [subviewArray objectAtIndex:0];
    self.categoryPop.mode = @"Deck";
    self.categoryPop.button2.titleLabel.text = @"ALL DECKS";
    self.categoryPop.button1.titleLabel.text = @"SELECT DECK";
    self.categoryPop.addButtonMode = @"NoAdd";
    [self.categoryPop style];
    self.categoryPop.delegate = self;
    self.categoryPop.label.text = @"Decks";
    [self.view addSubview:self.categoryPop];
    self.categoryPop.alpha = 0;
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.categoryPop.alpha = 1;
    [UIView commitAnimations];  
    
}

- (IBAction)resetPressed:(UIButton *)sender {
    
    NSMutableArray *result  = [[[[coreServices isDeckPresent:self.deckNameButton.titleLabel.text] toResult] allObjects] mutableCopy];
    if (result.count > 0) {
        for (ResultData *res in result) {
            [coreServices deleteResult:res];
        }
        [coreServices loadData];
        [self calculatePi:self.deckNameButton.titleLabel.text];
    }
    
}
- (IBAction)helpPressed:(id)sender {
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.helpPop.alpha = 1;
    [UIView commitAnimations];
}

- (IBAction)closeAd:(id)sender {
    self.closeAdButton.alpha=0;
    _bannerView.hidden=true;
}


//delegate for category
- (void) categoryFinished: (NSString *)textExtract{
    
    self.deckNameButton.titleLabel.text = textExtract;
    [self calculatePi:textExtract];
    [self.table reloadData];
    
}

-(void)calculatePi:(NSString *)deckName{
    
    int totalRight = 0;
    int totalWrong = 0;
    double totalStudied;
    int totalPictrue = 0;
    int totalText = 0;
    int totalCards = 0;

    NSDate *lastStudiedLocal;
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];


    if ([deckName isEqualToString:@"ALL DECKS"]) {
        for (DeckData *deck in [coreServices getDeckData]) {
            NSMutableArray *result  = [[[deck toResult] allObjects] mutableCopy];
            if (result.count > 0) {
                NSArray *sortedArray = [result sortedArrayUsingDescriptors:sortDescriptors];
                NSLog(@"%i",[[[sortedArray objectAtIndex:0] right] intValue]);
                NSLog(@"%i",[[[sortedArray objectAtIndex:0] wrong] intValue]);
                
                totalRight = totalRight+[[[sortedArray objectAtIndex:0] right] intValue];
                totalWrong = totalWrong+[[[sortedArray objectAtIndex:0] wrong] intValue];
                totalStudied = totalStudied + [[[sortedArray objectAtIndex:0] timeStudied] doubleValue];
            }
            //get number of total pictures and text
            for (CardData *card in deck.toCard) {
                if ([card.frontContains isEqualToString:@"text"]) {
                    totalText = totalText + 1 ;
                }
                else {
                    totalPictrue = totalPictrue + 1;
                }
                if ([card.backContains isEqualToString:@"text"]) {
                    totalText = totalText + 1 ;
                }
                else {
                    totalPictrue = totalPictrue + 1;
                }
            }
            totalCards = totalCards + deck.toCard.count;
        }
        lastStudiedLocal = [NSDate date];
    }
    else {
        NSMutableArray *result  = [[[[coreServices isDeckPresent:self.deckNameButton.titleLabel.text] toResult] allObjects] mutableCopy];
        if (result.count > 0) {
            //get history
            for (ResultData *res in result) {
                NSLog(@"timeStamp %@", res.timeStamp);
            }
            history =  result;
            
            NSArray *sortedArray = [result sortedArrayUsingDescriptors:sortDescriptors];
            for (ResultData *res in result) {
                NSLog(@"%@ %i",res.timeStamp,res.right.intValue);
                NSLog(@"%@ %i",res.timeStamp,res.wrong.intValue);
                totalRight = res.right.intValue+totalRight;
                totalWrong = res.wrong.intValue+totalWrong;
                totalStudied = totalStudied + res.timeStudied.doubleValue;

            }
            lastStudiedLocal = [[sortedArray objectAtIndex:0] timeStamp];
        }

        
        //get number of total pictures and text
        for (CardData *card in [[coreServices isDeckPresent:self.deckNameButton.titleLabel.text] toCard]) {
            if ([card.frontContains isEqualToString:@"text"]) {
                totalText = totalText + 1 ;
            }
            else {
                totalPictrue = totalPictrue + 1;
            }
            if ([card.backContains isEqualToString:@"text"]) {
                totalText = totalText + 1 ;
            }
            else {
                totalPictrue = totalPictrue + 1;
            }

        }
        totalCards = [[[coreServices isDeckPresent:self.deckNameButton.titleLabel.text] toCard] count];
    }

//convert to percentage
    double totalRightPercent = (double)totalRight / (double)(totalRight + totalWrong);
    double totalWrongPercent = (double)totalWrong / (double)(totalRight + totalWrong);
    
    //convert total study time to hour 
    _pieChart.layer.shadowOffset = CGSizeMake(0, 1);
    _pieChart.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    _pieChart.layer.shadowRadius = 3;
    _pieChart.layer.shadowOpacity = 0.8;
    
    PieChartView *result = [_pieChart initWithFrame:CGRectMake(0, 0, 70, 70)];
    if(result != nil){
        NSLog(@"Pie chart loaded");
    }
    [_pieChart clearItems];
	
	[_pieChart setGradientFillStart:0.3 andEnd:1.0];
	[_pieChart setGradientFillColor:PieChartItemColorMake(0.0, 0.0, 0.0, 0.7)];
	
	[_pieChart addItemValue:totalRightPercent withColor:PieChartItemColorMake(0.9, 0.5, 0.2, 1)];
	[_pieChart addItemValue:totalWrongPercent withColor:PieChartItemColorMake(0.18, 0.533, 0.227, 1)];
	
    self.rightPiLabel.text = [NSString stringWithFormat:@"%.1f",totalRightPercent] ;
    self.wrongPiLabel.text = [NSString stringWithFormat:@"%.1f",totalWrongPercent] ;
    self.totalCard.text = [NSString stringWithFormat:@"%i", totalCards];
    NSString *pictureTextFormat = [NSString stringWithFormat:@"%i", totalPictrue];
    NSString *textFormat = [NSString stringWithFormat:@"%i",totalText];
    self.pictureToTextRatio.text = pictureTextFormat;
    self.textRatio.text = textFormat;
    self.hoursStudied.text = [NSString stringWithFormat:@"%.f", floor(totalStudied/3600)];
    self.lastStudied.text = [NSString stringWithFormat:@"%@", [self dateFormatter:lastStudiedLocal]];
    
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarStatistics.png"]]  atIndex:0]; 
    
}

//configuring the tableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%i", history.count);
    return history.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    ResultData *res = [history objectAtIndex:indexPath.row];
    NSString *dateString = [res.timeStamp description];
    NSArray *dateArray = [dateString componentsSeparatedByString:@" "];
    NSLog(@"%i", dateArray.count);
    cell.textLabel.text = [self dateFormatter:res.timeStamp];//[NSString stringWithFormat:@"%@", [dateArray objectAtIndex:0]];    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.1f", (res.right.floatValue/(res.right.floatValue+res.wrong.floatValue))*100];
    return cell;
}

//date formatter
-(NSString *)dateFormatter:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    return [dateFormatter stringFromDate:date];
    
}

- (void)layoutAnimated:(BOOL)animated
{
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    
    
    CGRect bannerFrame = _bannerView.frame;
    bannerFrame = CGRectMake(0, 360, 320, 50);
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _bannerView.frame = bannerFrame;
    }];
}


- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
    self.closeAdButton.alpha =1;
    [self.view bringSubviewToFront:self.closeAdButton];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}


@end
