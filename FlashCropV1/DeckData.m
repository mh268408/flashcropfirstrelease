//
//  DeckData.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 7/31/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "DeckData.h"
#import "CardData.h"
#import "ResultData.h"
#import "UserData.h"


@implementation DeckData

@dynamic deckName;
@dynamic deckNumber;
@dynamic category;
@dynamic toUser;
@dynamic toCard;
@dynamic toResult;

@end
