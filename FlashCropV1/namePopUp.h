//
//  namePopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "alertPopUp.h"
#import <QuartzCore/QuartzCore.h>

@protocol NamePopUpDelegate <NSObject>
@required
- (void) NamePopUpFinished:(NSString *)textExtract :(BOOL)buttonIndex;
@end

@interface namePopUp : UIView <UITextFieldDelegate>
{
	id <NamePopUpDelegate> delegate;
}
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) NSString *button1Title;
@property (weak, nonatomic) NSString *button2Title;
@property (weak, nonatomic) NSString *type;

@property (retain) id delegate;
@property (strong, nonatomic) IBOutlet UITextField *textBox;
@property (strong, nonatomic) IBOutlet UIView *content;
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) alertPopUp *alertPop;
@property bool cancel;
- (IBAction)closePopUp:(id)sender;
- (IBAction)button1Pressed:(id)sender;
- (IBAction)button2Pressed:(id)sender;
- (void) style;
@end
