//
//  StudyNowStatisticsViewController.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "FCUIView.h"
#import "coreServices.h"

@interface StudyNowStatisticsViewController : FCUIView
@property (nonatomic, strong) IBOutlet UIView *backgroundView;
@property (nonatomic, strong) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *shuffleCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *startFromBeginingCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *prioritizeIncorrectCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *prioritizeFavoriteCheckBox;

@property UIColor * grey;
@property UIColor * green;
@property UIColor * orange;

- (IBAction)shuffleCheckBoxPressed:(id)sender;
- (IBAction)startFromBeginningCheckBoxPressed:(id)sender;
- (IBAction)prioritizeIncorrectCheckBoxPressed:(id)sender;
- (IBAction)prioritizeFavoriteCheckBoxPressed:(id)sender;




@end
