//
//  CreateCardSettingsViewController.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/2/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "CreateCardSettingsViewController.h"

@interface CreateCardSettingsViewController ()

@end

@implementation CreateCardSettingsViewController
@synthesize backgroundView;
@synthesize containerView;
@synthesize autoNameCheckBox;
@synthesize namePrefixBox;
@synthesize coreData;
@synthesize grey;
@synthesize green;
@synthesize orange;


bool autoName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navBar = true;
    self.back = true;
    self.title = @"CREATE CARD";
    self.grey = [UIColor colorWithRed:0.388 green:0.329 blue:0.4 alpha:1];
    self.green = [UIColor colorWithRed:0.184 green:0.533 blue:0.277 alpha:1];
    self.orange = [UIColor colorWithRed:0.96 green:0.501 blue:0.203 alpha:1];
    [super viewDidLoad];
    

	// Do any additional setup after loading the view.
    self.namePrefixBox.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for(UIView *view in self.tabBarController.tabBar.subviews) {  
        if([view isKindOfClass:[UIImageView class]]) {  
            [view removeFromSuperview];  
        }  
    }  
    
    if ([[[[coreServices getUserData] objectAtIndex:0] autoDeckname] isEqualToNumber:[NSNumber numberWithInt:1]]){
        autoName  = true;
    }
    else {
        autoName  = false;
    }
    if(autoName){
        self.autoNameCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
    }
    else{
        self.autoNameCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
    }
    
    [self.tabBarController.tabBar insertSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavBarSettings.png"]]  atIndex:0]; 
}

- (void)viewDidUnload
{
    [self setBackgroundView:nil];
    [self setContainerView:nil];
    [self setNamePrefixBox:nil];
    [self setAutoNameCheckBox:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    //save to coreservice
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSDictionary *changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        self.namePrefixBox.text, @"cardNamePrefix", 
                                 nil];
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"cardNamePrefix",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
    
    
    return YES;
}
- (IBAction)autoNameCheckBoxPressed:(id)sender {
    
    NSNumber *positive = [[NSNumber alloc] initWithInt:1];
    NSNumber *negative = [[NSNumber alloc] initWithInt:0];
    NSDictionary *changeValue;
    
    if(autoName){
        self.autoNameCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxPassive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        negative, @"autoDeckname", 
                       nil];
    }
    else{
        self.autoNameCheckBox.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"checkBoxActive.png"]];
        changeValue = [[NSDictionary alloc] initWithObjectsAndKeys:                                        positive, @"autoDeckname", 
                       nil];
    }
    autoName = !autoName;
    //save Settings in core service
    NSDictionary *changeFlag = [[NSDictionary alloc] initWithObjectsAndKeys:                                positive, @"autoDeckname",  
                                nil];
    [coreServices changeSettings:changeValue :changeFlag];
}
@end