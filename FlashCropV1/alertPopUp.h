//
//  alertPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol alertPopUpDelegate <NSObject>
@required
- (void) alertPopUpFinished :(bool)confirmed;
@end

@interface alertPopUp : UIView
{
	id <alertPopUpDelegate> delegate;
}
@property (retain) id delegate;
@property (strong, nonatomic) IBOutlet UILabel *alertMessageLabel;
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UIView *content;
- (IBAction)closePopUp:(id)sender;
- (IBAction)confirm:(id)sender;
- (void) style;
@property (weak, nonatomic) IBOutlet UIButton *buttonOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwo;

@end
