//
//  welcomePopUp.h
//  FlashCropV1
//
//  Created by Supradeep Vijaya Kumar on 8/11/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "coreServices.h"

@protocol WelcomeDelegate <NSObject>
@required
- (void) welcomeFinished;
@end


@interface welcomePopUp : UIView <UITextFieldDelegate, UIImagePickerControllerDelegate>
{
	id <WelcomeDelegate> delegate;
}

@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UIView *welcomeBox;
@property (weak, nonatomic) IBOutlet UITextField *nameBox;
@property (weak, nonatomic) IBOutlet UITextField *emailBox;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (retain) id delegate;
- (IBAction)choosePicturePressed:(id)sender;
- (IBAction)donePressed:(id)sender;
- (IBAction)skipPressed:(id)sender;

-(void) style;
@end
