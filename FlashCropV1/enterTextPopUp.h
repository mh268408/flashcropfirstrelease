//
//  enterTextPopUp.h
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol enterTextDelegate <NSObject>
@required
- (void) enterTextFinished: (NSString *)textExtract;
@end

@interface enterTextPopUp : UIView <UITextViewDelegate>
{
	id <enterTextDelegate> delegate;
}
@property (retain) id delegate;
@property (strong, nonatomic) IBOutlet UITextView *textBox;
@property (strong, nonatomic) IBOutlet UIView *background;
@property (strong, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (IBAction)donePressed:(id)sender;
- (IBAction)closePopUp:(id)sender;
- (void) style;

@end
