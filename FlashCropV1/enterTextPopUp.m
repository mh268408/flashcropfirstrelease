//
//  enterTextPopUp.m
//  FlashCropV1
//
//  Created by Max Heckel on 8/1/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "enterTextPopUp.h"
#import <QuartzCore/QuartzCore.h>


@implementation enterTextPopUp
@synthesize background;
@synthesize content;
@synthesize closeButton;
@synthesize textBox;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}


- (IBAction)donePressed:(id)sender {
    
    [[self delegate] enterTextFinished:self.textBox.text];
    [self.textBox resignFirstResponder];
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
    
}

- (IBAction)closePopUp:(id)sender {
    [self.textBox resignFirstResponder];
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.5];
    self.alpha = 0;
    [UIView commitAnimations];
}

- (void) style{
    
    self.textBox.delegate = self;
    content.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"homeBackground.png"]];
    
    self.content.layer.borderColor = [UIColor whiteColor].CGColor;
    self.content.layer.borderWidth = 1;
    background.backgroundColor = [UIColor colorWithWhite:0 alpha:0.75];
    self.textBox.backgroundColor = [UIColor colorWithWhite:1 alpha:0.75];
    self.textBox.layer.borderColor = [UIColor colorWithWhite:0 alpha:1].CGColor;
    self.textBox.layer.borderWidth = 2;
    
}


-(void)resignKeyboard{
    [self.textBox resignFirstResponder];
}


@end
