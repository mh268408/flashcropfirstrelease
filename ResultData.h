//
//  ResultData.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 9/17/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DeckData;

@interface ResultData : NSManagedObject

@property (nonatomic, retain) NSNumber * right;
@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSNumber * timeStudied;
@property (nonatomic, retain) NSNumber * wrong;
@property (nonatomic, retain) DeckData *toDeck;

@end
