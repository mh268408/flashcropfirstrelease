//
//  CardData.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 9/16/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "CardData.h"
#import "DeckData.h"


@implementation CardData

@dynamic backContains;
@dynamic backImage;
@dynamic backOriginal;
@dynamic backText;
@dynamic cardNumber;
@dynamic favorite;
@dynamic frontContains;
@dynamic frontImage;
@dynamic frontOriginal;
@dynamic frontText;
@dynamic right;
@dynamic wrong;
@dynamic createdAt;
@dynamic toDeck;

@end
