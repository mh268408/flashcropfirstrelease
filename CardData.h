//
//  CardData.h
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 9/16/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DeckData;

@interface CardData : NSManagedObject

@property (nonatomic, retain) NSString * backContains;
@property (nonatomic, retain) id backImage;
@property (nonatomic, retain) id backOriginal;
@property (nonatomic, retain) NSString * backText;
@property (nonatomic, retain) NSNumber * cardNumber;
@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSString * frontContains;
@property (nonatomic, retain) id frontImage;
@property (nonatomic, retain) id frontOriginal;
@property (nonatomic, retain) NSString * frontText;
@property (nonatomic, retain) NSNumber * right;
@property (nonatomic, retain) NSNumber * wrong;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) DeckData *toDeck;

@end
