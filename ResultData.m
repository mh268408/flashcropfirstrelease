//
//  ResultData.m
//  FlashCropV1
//
//  Created by Arkopaul Sarkar on 9/17/12.
//  Copyright (c) 2012 FlashCrop LLC. All rights reserved.
//

#import "ResultData.h"
#import "DeckData.h"


@implementation ResultData

@dynamic right;
@dynamic timeStamp;
@dynamic timeStudied;
@dynamic wrong;
@dynamic toDeck;

@end
